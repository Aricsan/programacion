# Atajos de teclado para VIM
## Sin modo insercion
gg -> ir al principio del fichero  
G -> ir al final del fichero  
nºG || nºH -> ir a la linea indica en el numero  
^F -> avanzar una pantalla  
^B -> retroceder una pantalla  
^U -> subir media pantalla  
^D -> bajar media pantalla  
U -> volver atras  
O -> irse a la linea de encima
\- -> comienzo de la linea anterior  
\+ -> comienzo de la linea siguiente  
H -> ir a la primera linea visible en pantalla  
L -> ir a la ultima linea visible en pantalla  
M -> ir al medio demla pantalla  
w || W -> avanzar palabra por palabra  
e || E-> se situa al final de una palara  
b || B-> se situa al pricipio de una palara  
h -> movimiento a la izquieda  
l -> movimiento a la derecha  
j -> movimiento abajo  
k -> moviento arriba  
dd -> borra la linea entera  
^v || ^V -> entrar en modo visual  
* copiar -> selecionar el texto y pulsar c  
* pegar -> pulsar p  

Altgr + ñ -> cambiar una caracter minuscula por su caracter mayuscula y viciversa  
r + algo -> reemplaza algo por algo  

# Crear una macro
qnº -> para crear una macro   
@nº -> para ejecutar la macro  
/nombre -> busca la primera palabra nombre del documento  

# Configuracion de .vimrc
set nu -> enumerar las lineas  
set cindent -> meter sangria, inventar o maquetar  
set sw=4 -> cuantos espacios para la sangria  
set expandtab -> convierte las tabulaciones en espacios  
colorscheme distinguished -> cambiar el tema a vi  
syntax on -> detecta palabras de c y las cambia de color  
set t\_Co=256 -> la cantidad de colores que puede usar  
source ~/.vim/vundle.vim -> coge un fichero y lo incrusta ahi  

# Ordenes intresantes
PluginInstall -> instalar los puglins  
digraph -> crear diagramas, nombre -> nombre;  
:r fichero.cpp -> me crea el esqueleto de un programa  
