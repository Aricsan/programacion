# Variables Static
- Las variables de tipo static son variables que tienen su visibiliad, son conocidas, en el ambito en el que se han creado. En el caso de ser declaradas en una funcion su visibilidad se restinge al ambito de dicha funcion pero su ambito o scope(alcance) es de todo el programa, hasta que este no acabe la variable junto con su valor sigue existiendo, mantiene su valor entre llamadas. Si la variable es declarada fuera de una funcion, en el fichero, tanto su ambito como su visibilidad se restingen al fichero  
- La diferencia entre variables estaticas y globales es que la visibilidad y el ambito de estas ultimas siempre se conocen en todo el fichero, ademas de conocerse en ficheros externos  
- Si realizamos varias llamadas a una función con una variable estatica, esta mantiene su valor inicial en cada llamada, return se encargara de juntar todos los valores de retorno y asignarselo a dicha variable. 

# Recursividad
Consiste en hacer que una funcion se llame a si misma, para evitar un bucle infinito es necesario una condicion de parada o contorno  
    - La ventaja de la programacion recursiva es la velocidad, es mucho mas rapida que la programación iterativa  
    - La desventaja es el alto consumo de memoria, ya que en cada llamada a la funcion se crea una nueva pila con las variables locales de esta  
Al encontrar la condición de parada se libera la pila y se recogen los valores de retorno de las sucesivas llamadas, empezando por la ultimas y subiendo en la pila   
Las variables estaticas son las unicas que permanecen entre llamadas, ya que no se encuentran en la pila de cada llamada  

# Main
- argv --> Es una array de caracteres que recibe y almacena los argumentos que el usuario introduce al ejecutar el programa, el ultimo argumento sera un NULL  
- argc --> Es una variable de tipo entero que almacena el total de argumentos introducidos, como minimo almacenara un 1, por el nombre del programa  
