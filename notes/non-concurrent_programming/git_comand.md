# Parametros de Git
    Clone -> bajarse un respositorio de la nube  
    push -> subir los combios a la nube  
    pull -> bajarse los cambios de la nube  
    add -> para empaquetar los cambios que quieres guardar  
    commit -> mandar el paquete al repositorio local  
    stash -> cajon temporal, guardar los cambios que no quieres subir temporalmente  
    it add.-> guarda todos los cambios de la carpeta actual y las subcarpetas de esta  
    git mv -> para renombrar, afecta al remoto  
    git rm -> para borrar, afecta al remoto  
    git add -u -> los ficheros que no esten es este paquete desapareceran del remoto  
    git status -> nos enseña los ficheros que han cambiado  
    git diff -> cambios que ha habido dentro de un fichero  
    git log -> todos los cambios con mensajes  
    git commit -m "mensaje" -> para dar un mensaje de que hemos cambiado en este commit, si quitamos el -m seria redactar un email  
    init -> crear un repositorio desde cero  
    git branch -a -> nos enseña las ramas que hay, sin -a nos enseña la rama en la que estamos  
    git checkout -b nombre -> crear una rama nueva, sin la b cambias de rama  
    git merge -> fusionar ramas  
