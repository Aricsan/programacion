# Operadores Logicos
## XOR (+)
| Entrada | Entrada | Salida |
|:-------:|:-------:|:------:|
| 1       | 1       | 0      |
| 1       | 0       | 1      |
| 0       | 1       | 1      |
| 0       | 0       | 1      |

## AND &
| Entrada | Entrada | Salida |
|:-------:|:-------:|:------:|
| 1       | 1       | 1      |
| 1       | 0       | 0      |
| 0       | 1       | 0      |
| 0       | 0       | 0      |

## OR +
| Entrada | Entrada | Salida |
|:-------:|:-------:|:------:|
| 1       | 1       | 1      |
| 1       | 0       | 1      |
| 0       | 1       | 1      |
| 0       | 0       | 0      |

## NOT -
| Entrada | Salida  | 
|:-------:|:-------:|
| 1       | 0       |
| 0       | 1       | 

# Depurar un Programa
compilar con g++ -g -> crea un formato de depuracion nativo para el sistema   
g++ -S fichero.cpp -> obtener el ensamblador de un codio  
para ver el programa en codigo ensamblador g++ -s --disassemble || -D ejecutable > archivo de salida.s  
gdb -q nombre del programa -> ejecutar el debugger, -q para que no nos muestre el mensaje  
run || r -> ejecuta el programa  
break || b -> numero de linea o nombre de funcion poner un punto de ruptura  
b 13 if i ==120 -> pone un break en la linea 13 si i vale 120  
next || n || enter -> ejecuta la siguiente linea de codigo  
step -> cuando hay una llamada a una funciona la ejecuta paso por paso  
continue -> ejecuta la instruccion entera  
info breakpoint -> muestra los breakpoints  
quit || q -> para salir 
x /c nombre -> la x examina la variable, lo que haya en esa direccion de memoria, /c es que lo interprete como caracter y el nombre de la variable  
backtrace -> muestra la funcion en la nos encontramos y cual la ha llamado  
list || l -> lista el codigo, imprime de 10 en 10 lineas  
- 2,5 -> imprime las lines que se encuentran en ese ambito, si quitamos el 5 imprime las 10 siguientes  

print || p variable -> nos muestra el valor de la variable  
- variables locales pueden no valer 0  
- variables globales siempre valen 0  
- &variable -> imprime el offset de la variable

# Estructura de un Programa
call -> incrementa el registro IP en x y mete el contenido de IP en la pila  
offset -> direccion de memoria  
inicializar || declarar -> reservar un espacio en memoria  
definir -> dar valor a ese espacio  
little endian -> leer datos de izquierda a derecha  
big endian -> leer datos de derecha a izquierda  

# Tipos de Anotación
underscore -> todo en miniscula y las palabras compuestas separas por \_ (se representa en minuscula)  
Anotacion de camello -> el primer caracter de las palabras en mayuscula  

# Mis Anotaciones
Asignacion por defecto el underscore  
Las constantes en mayuscula  

# Comandos Intresantes
doxygen -> crea la documentacion a partir de los comentarios, para ello es necesario escribir los comentarios dentro de /\*\*comentario\*/  
- doxygen -g codigo.cpp -> crea una copia de seguridad  
- doxygen codigo.cpp -> crea la documentacion 

g++ -Dconstante=\'a\' mays.cpp -o mays-> definir una constante al compilar, es este caso de tipo char
