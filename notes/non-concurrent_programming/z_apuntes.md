# Keywords C
auto -> en c se usa para definir que una variable es local dentro de su bloque (funcion). Por defecto todas las varibles son locales por lo que es inutil. En c++ indica que la variable coge el tipo de su inicializador, es decir, auto var = 1.5; var sera de tipo double  
const -> define una constante que no modifica su valor  
\#define -> define una _macro_, sustituye un nombre por un valor  
enum -> define una serie de cadenas, cada cadena es enumerada empezando por el 0. Ha dicha cadena le asignamos un nombre, este nombre representa la cadena, para representar cada cadena en concreto le asignamos un nombre al nombre de la cadena. A este nombre le asginamos la cadena que queremos representar y la mostramos. enun se utiliza para enumerar cadenas  
extern -> importar variables o funciones externas  
signed -> indica que el primer bit es de signo, se aplica por defecto  
unsigned -> indica que no hay bit de signo  
sizeof -> indica cuantos bytes ocupa una variable o tipo de dato  
struct -> define una variable que almacena variables. Por ejemplo una variable llamada persona que tenga como variables nombre, DNI, y años. Se utiliza para representar mediante una variable varios tipos de datos  
volatile -> son variables que pueden ser modificadas fuera del programa  
'a' -> Las comillas simples convierten un caracter a su respectivo valor hexadecimal del codigo ascii  
{} -> se utiliza para crear bloques dentro de otros, bloques anidados
! -> equivale a not  
^ -> equivale al xor  

# Información General
- El parametro formal es el nombre de la variable o BP+4 por ejemplo si es un entero  
- El parametro actual es el valor de la variable  
- El ambito es el espacio donde la variable tiene uso (corresponde con la funcion o bloque)
- La visibilidad es el espacio donde se puede imprimir la variable
- od -txlc fichero.cpp -> me muestra las direcciones de memoria y cuando acupan en ella las variables
- La , es un operando mas, se hace lo ultimo y toma el valor del ultimo parametro u operacion en ejecutarse, por ejemplo, es esta sintaxis a = 4,5 a vale 4 porque el ultimo valor que es el 5 no se asigna a nada, se pierde. En esta otra, a = (4,5) a valdra 5, porque la , al ser lo ultimo en ejecutarse toma el valor 5 o el valor de retorno de la ultima operacion, aqui al no haber operaciones pues el 5 se queda como esta
- Las comillas dobles se usan para las cadenas de caracteres mientras que las simples para caracteres  
- upcasting -> cambiar de base un numero automaticamente
- operador unario -> -3
- operados binario -> 3 + 5
- El ++ ejecuta las operaciones de derecha a izquierda y luego incrementa la variable.
si ponemos el ++ despues de una variable, este se pone a la cola el ultimo, es decir, primero se realizan todas las demas operaciones y por ultimo el ++. Si no hay mas operaciones el ultimo es el primero por lo que realiza el incremento. Si esta antes de una variable sigue ejecutandose al final sin embargo como la siguiente operacion afecta al ++ porque este empieza de derecha a izquierda tiene que realizar el incremento si o si para hacer la operacion.    
- En la arquitectura x64 las direcciones de memoria ocupan 8 bytes asi tenemos mucha mas memoria que enumerar, en la arquitectura x32 las direcciones de memoria ocupan 4 bytes  
- La diferencia entre un puntero y un array es que al crear el puntero no especificamos las celdas ni las dimensiones que va a ocupar, mientras que con un array si, ya que debemos de inicializar las celdas especificando asi las dimensiones y por lo tanto lo que va a ocupar. Si hacemos un sizeof de un puntero nos dira que ocupa 8 bytes, mientras que un array nos dira la cantidad de bytes que ocupan sus celdas.
- indice -> es la posicion de un dato en un array  
- hoisting -> tecnica de optimización de codigo, por ejemplo, sacar operaciones que se repitan en un bucle mejorando asi el rendimiento en tiempo de ejecución, se pueden sacar siempre y cuando esas operaciones no modifiquen el resultado  
- \*\*a = b -> a es un puntero que apunta a otro puntero, para ello b tiene que ser un puntero doble, ya que, \*a apunta al contenido de b y \*\*a apunta al contenido de \*b[0], que es la dirección de memoria de la cadena de datos, al iniciarlizar estos punteros siempre van a apuntar a la cadena y celda 0, no podemos acceder a una celda diferente, para ello deberiamos de incrementar el puntero o en el printf especificar que cadena imprimir con []  
- EL tipo de dato char normalmente almacena caracteres en un formato codificado como ASCII, sin embargo, como almacena 1 byte es posible almacenar numeros siempre y cuando no sobrepasen el byte, es decir, como mucho el numero 255  
- NULL -> es la direccion 0 de memoria, se usa para indicar el fin de una cadena de cadenas, mientras que 0 o \0 se usan para indicar el fin de una cadena de caracteres, 0 y \0 se usan indistintamente ya que tienen todos los bits a 0, aunque es recomendable usar \0 por legitividad. NUll sin embargo es distinto ya que es una direccion de memoria   
- Si al inicializar un puntero lo igualamos a otro puntero no debemos poner ni * ni &, con * estariamos dandole el valor de la variable a la que apunta el puntero, cuando lo que queremos es darle la dirección de memoria que contiene el puntero, si este fuese un puntero doble, si funcionaria. Al igualar punteros se igualan sus contenidos, ya que un puntero apunta a la dirección de memoria de otro puntero y con el * da el paso de meterse dentro y quedarse con su valor que es la dirección de memoria que nos interesa, la que apunta a la variable. Con & estariamos diciendo que el puntero almacena la direccion de memoria de otro puntero, es decir un puntero de punteros o doble y para eso ya esta ** , ademas al igualar punteros no tiene sentido que le demos la dirección de memoria del puntero, sino la dirección de memoria que contiene el puntero. En ambos casos daria error  
- Solo los punteros de tipo char se pueden inicializar con valores, caracteres, los demas deben ser asignados a la dirección de una variable que contenga un valor del mismo tipo, si quiero mas de un valor tengo que declarar un array  

# Paso por Referencia
En una función si recibimos una direccion de memoria estamos haciendo un paso por referencia, es util para cuando queramos modificar el valor de una variable, ya que, toda modificación que se le aga al puntero de la función que apunta esta variable afecta a la misma variable
- Podemos indicarlo con un el nombre del puntero o con &, siendo el nombre lo que va a reecibir es la direccion a la que apunta y & la direccion del puntero  

# Paso por Valor
Si en una función recibimos un valor estamos haciendo un paso por valor, simplemente le estamos pasando el valor de una variable, quedando esta intacta.  
- Si la variable es un puntero su valor se pasa con * si no, el nombre de la variable  

# Realloc
- Realloc es una función que dvuelve un void * por eso el molde, lo que hace es reservar celdas del tipo especificado y asignarselo a un puntero, amplia una reserva de memoria.
Si queremos reservar cadenas de caracteres en un puntero, es necesario que este sea del tipo \*\*, si es de tipo * el realloc reservara las celdas necesarias sin embargo no hay manera de indicarle en que celda escribir ni tampoco mostrar, porque no disponemos de una celda para indicar en que dirección escribir o mostrar, aunque incremente el puntero estaria incrementando una celda de char. En el caso de que sea un puntero \*\* al asignarle las cadenas debemos de indicarle la dirección de memoria de esta, es decir, con un %ms, asi asignariamos la dirección de la cadena en la celda 0 del puntero. Con %s asignariamos la cadena y nosotros queremos su dirección.
- Malloc realiza una reserva de memoria y te devuelve un puntero, que guardas en una variable, por lo tanto no es necesario que dicha variable este inicializada, le da igual. Sim embargo, realloc, al ampliar una reserva de memoria, la variable debe estar inicializada, ya que sino estariamos pasando un puntero aleatorio, puntero salvaje. Si la variable esta inicializada a NULL la primera vez actuaria como un malloc.  

# Moldes
Los moldes se utilizan para convertir un tipo de dato a otro. Se definen mediante () de la siguiente manera, (tipo de dato convertido) tipo de dato a convertir.  
Al convertir un entero a char coge solo el primer byte del entero, es decir, coge solo los primeros 255 numeros siempre y cuando el char sea unsigned. El char almacena el numero, es decir, se puede operar con el.  

# Scanf
La funcion scanf funciona de la siguiente manera:  
Recibe como parametro el dato que el usuario introduce y como argumento un puntero _temporal_ que apunta a la direccion de memoria de dicho dato. El & se encarga de almacenar el dato introducido por el usuario en la direccion de memoria del puntero _temporal_. Si el puntero _temporal_ es un puntero en si, no hace falta el & ya que al ser un puntero no queremos que el dato se almacene en su direccion de memoria, que es lo que pasaria si lo pondriamos, se convertiria en una variable normal.  

El scanf convierte la variable en un puntero que apunta a la direccion de memoria en al que el dato se guarda. despues con & ese dato lo introducimos en la direccion de memoria de la variable y nos devuelve la variable con el valor introducido. 

Si ponemos * en vez de & estariamos diciendo que la variable o puntero _temporal_ apunta a otro puntero, cosa que no tendria sentido porque el puntero _temporal_ apunta al valor introducido.

Si al ejecutar el scanf no hay nada en el stdin scanf lo pedira, sin embargo, si hay, no pedira valores porque ya tomara los que haya en stdin. Al leer esos valores digamos que desaparecen, o dejan de estar disponibles para las funciones que lean de stdin, de tal forma que si tengo un scanf e inserto un 5 y luego tengo otro scanf me pedira que inserte otro numero ya que ese 5 digamos que esta cogido. Si queremos que los dos scanf lean el mismo numero debemos de volver a meterlo en stdin al ejecutar el primer scanf.  

La variable que nosotros definimos en el scanf, en el caso de que sea un puntero siempre apuntara a la posición 0 o celda 0 en el caso de que sea un array, si queremos insertar un dato en la celda 0 no hace falta el &, sin embargo si queremos insertar datos en una celda distinta a 0 nos hace falta indicarla mediante [] y ademas poner el &, si no ponemos el & estariamos almacenando el dato en la posicion 0 porque la variable del scanf siempre apuntara a la posicion 0, incluso si incrementemos el puntero antes del scanf. Hay que poner el & siempre y cuando queramos insertar un dato en una posicion distinta a 0.  

# Punteros
Los punteros son variables que almacenan una dirección de memoria. Hay dos maneras de definir punteros en C.  
- char a[]  
- char \*a  

El * indica que lo que viene es un puntero y ademas decimos que nos diga a donde apunta dicho puntero, que sera lo que tiene esa direccion de memoria  

La direccion de memoria apunta al principio de la cadena de caracteres y sigue hasta encontrar un 0, que es cuando finaliza. Para imprimir los caracteres de un puntero utilizamos %c o %s dependiendo de lo que queramos imprimir:
- %s Imprime lo que tiene el puntero  
- %c Imprime lo que tiene la variable  

Al imprimir con %s no hace falta determinar con * que la variable es un puntero, ya que %s significa imprimir la cadena de caracteres y en C una cadena de caractreres solo es posible mediante un puntero, pr lo tanto el compilador identifica que es un puntero y lo trata como tal.  

Sin embargo, al imprimir con %c si es necvesario indicar con * que la variable es un puntero, ya que sino, imprimiria la direccion de memoria en forma de caracter. Si podemos * imprimira el primer caracter de la cadena, para imprimir el segundo debemos de sumarle 1 al puntero, asi sucesivamente  

# Puntero de Punteros
Un puntero de punteros o cadena de cadenas de caracteres, es un puntero que apunta a otro puntero y este apunta al principio de una cadena de caracteres. Se define de la siguinte manera:
- char \*a[]

- Si queremos imprimir un caracter con %c debemos de indicar que nos imprima lo que contiene el puntero de la posicion 0.
- Si queremos imprimir una cadena de caracteres con %s debemos de indicar que nos imprima lo que contiene la direccion del puntero.
- Si queremos imprimir cadenas de caracteres mediante notacion de punteros, debemos de crear un puntero que apunte a este, ya que, si incrementasemos el puntero que contiene las cadenas estas se quedarian huerfanas, el puntero siempre debe de apuntar a la primera celda.  

# Aritmetica de punteros
Se le llama aritmetica de punteros a las operaciones que se realizan por debajo a la ora de realizar operaciones con punteros, por ejemplo, sumarle 1 a un puntero o incrementarlo realmente es multiplicar ese 1 por el sizeof del puntero, es decir 8 bytes, por lo tanto al incrementar un puntero no se le suma 1 se le suma 8. Lo mismo pasa si incrementamos el contenido de un puntero, si es de tipo char se incrementa en 1, sin embargo, si es de tipo entero se incrementara en 4  

# Funciones Teoria
- Los () en los punteros excluyen, es decir, int (\*)[10] seria un puntero a un puntero a un array de 10 enteros.  
- Tabla de simbolos -> se crea al compilar y es utilizada por el compilador para indicar la dirección de memoria de las etiquetas(nombres de las funciones).
- Las funciones son punteros que apuntan a su codigo ejecutable, es asi porque al crear una función no se sabe cuanto espacio ocupan. AL indicar el nombre de la funcion junto con () se esta indicando que ejecute la dirección de memoria a la que apunta

# Operadores taquigraficos
- %=
- <<=
- \>\>=

# Macros de texto
\#define P(...) printf ("Mensaje de macro " \_\_VA\_ARGS\_\_)

# Funciones
- fflush (stdin) -> vacia el buffer guardandolo en el disco  
- fflush (stdout) -> vacia el buffer en la salida, el buffer se vacia solo si esta lleno, hay un salto de line (\n) o fflush (stdout)  
- scanf -> introducir datos por teclado scanf("tipo de dato", &variable)  
- system ("comando") -> ejecuta comandos del sistema operativo  
- fprintf (fichero || tubo, "mensaje") -> nos permite escribir en un fichero o tubos, (stderr, stdout, stdin)  
- getchar() -> lee un solo caracter de stdin y devuelve el valor ASCII numerico correspondiente 
- getc (tubo) -> equivalente a getchar con la diferencia de poder leer de cualquier flujo de entrada  
- ungetc (valor ASCII numerico, stdin) -> lee el valor ASCII numerico y lo devuelve en forma de caracter al fujo de entrada (stdin)  

# Dividir en varios archivos un programa
La definición de las funciones hay que moverlas a un archivo .cpp, por cada archivo .cpp que creemos debemos de crear otro .h, en este definimos solo las llamadas a las funciones, sus declaraciones.  
- Para que no se importe la libreria mas de una vez y se produzca un bucle en el proceso de preprocesado hay que indicar mediante la directiva de prepocesado \#ifndef una constante que no interfiera con ninguna otra \_\_FICHERO\_H\_\_, la directiva se cierra con \#endif, esto hay que hacerlo en todos los .h.  
- Si se usa el compilador g++ debemos de comprobar mediante la directiva \#ifdef si existe la constante \_\_cplusplus, si existe significa que estamos usando g++ ya que este crea dicha constante al compilar, si es asi debemos de indicar que cree extern "C" ya que asi g++ no podra cambiar el nombre a las funciones.  
- Las constantes globales hay que definirlas en otro archivo.h, los .h no pueden tener codigo. Las constantes definidas aqui, no pueden cambiar su definicion, es decir, si tengo una variable global denominada a no puedo cambiar su valor o definición, si lo ago el compilador dara un error de multiples definiciones, ya que a tendria mas de un valor y eso no puede ser. Por lo tanto, solo puedo tener variables globales que nunca van a cambiar.
- En el .cpp del main llamamos solo a las librerias .h, ya que son las que no tienen codigo.  
- Para compilar los .cpp hacemos g++ -c fichero.cpp ...
- Para linkar los codigo objeto g++ -o ejecutable fichero.o ...
- Si en los includes indicamos una libreria entre <> el compilador buscara en el sistema sin embargo si la indicamos entre "" la buscara en el directorio, si no encuentra la libreria la buscara en el sistema o viceversa.  


# Librerias
## \#include\<stdio\_ext.h>  
- \_\_fpurge (stdin) -> vacia el buffer sin guardarlo devolviendo un cero, \_fpurgue no devuelve nada  
- \_\_fpurge (stdout) -> vacia el buffer de salida  

## \#include\<string.h>
- strcpy (destino, origen) -> copia la cadena origen a la cadena destino, es una funcion insegura ya que si destino tiene una longitud mayor se sobreescribe el codigo y se podria obtener privilegios  
- strncpy (destino, origen, tamaño maximo) -> soluciona el problema de strcpy indicando el maximo de la cadena destino  
- strcat (destino, origen) -> concatena origen con destino, tenemos el mismo problema que strcpy    
- strncat (destino, origen, maximo - lo que ya ocupa destino (strlen)) -> concatena origen con destino hasta que destino llegue al maximo  
- strlen (cadena) -> devuelve la longitud de una cadena sin contar el \0  
- bzero (string/puntero, tamaño (sizeof)) -> rellena todo el tamaño indicado del puntero o string con \0  
- memset (string/puntero, valor, tamaño (sizeof)) -> rellena el tamaño indicado del puntero o string con el valor especificado  

## \#include\<math.h>  
- Para realizar operaciones con potencias llamamos a la funcion pow(numero, potencia)  
- Para realizar raices cuadradas sqrt(numero o variabe)  
- para obtener el valor absoluto de un número utilizamos fabs(numero), el valor absoluto de un numero es su valor sin tener en cuenta su signo  

## \#include\<stdio.h>  
### Printf
- \\n salto de linea  
- \\r sobreescribe el printf  
- %3.2i -> el numero de la izquierda representa la cantidad de espacios, si es sustituido por 0 o . o .0 no habra espacios, el numero de la derecha la cantidad de ceros, si es sustituido por . o .0 no añadira ningun 0 ni tampoco lo mostrara
- %g,G -> muestra un float con los decimales correspondientes, sin ceros añadidos  
- %e,E -> muestra un float en notacion cientifica
- %% -> nos muestra un % en pantalla  
- %u -> muestra un unsigned int  
- %i -> muestra un numero en base decimal signed, si la variable es de tipo unsigned tomara el ultimo bit como signo, por lo tanto, aun siendo la variable unsigned mostrara el numero en formato signed, por defecto muestra el numero en base decimal   
- %d -> muestra un numero en base decimal con signo  
- %o -> muestra un numero en base octal  
- %x,X -> muestra un numero en base hexadecimal  
- %f -> muestra un numero de coma flotante  
- %c -> muestra un caracter  
- %s -> muestra una cadena de caracteres  
- %hi -> muestra un short int  
- %li -> muestra un long int  
- %Li -> muestra un long long int  
- %Lf -> muestra un double  
- %p -> imprime la dirección a donde apunta un puntero, si la variable es un puntero dira la dirección de memoria a la que apunta y si queremos mostrar la dirección de memoria de una variable debemos indicarlo con &   

### Scanf
- %a -> equivalente a %f en el estandar C99, sino funciona como un %m  
- %e,E -> numero en notacion cientifica, equivañente a %f  
- %g,G -> numero con los decimales justos, equivale a float %f  
- %hi -> h es short  
- %li -> l es long  
- %Li -> L es long long  
- %lf -> long float, double  
- %d -> lee solo un numero en base decimal  
- %i -> puede leer en base decimal, hexadecimal 0x... y octal 0...  
- %o -> lee solo numeros en octal  
- %u -> lee numeros sin signo  
- %x -> lee numero en hexadecimal  
- %s -> lee una cadena de caracteres 
- %c -> lee un caracter  
- %p -> lee una dirección de memoria  
- %n -> cuenta todos los caracteres leidos de stdin, no para de contar hasta encontrarse con un caracter, lo almacena en un int  
- %\*i-> * es el caracter de supresion de asignacion, lee pero no asigna   
- %[] -> entre [] indicamos los elementos que va a leer, si queremos que leo todo menos lo indicado [^]. Los elementos especiales como el \- hay que ponerlo al final y el ] al principio.  
- %m -> hace un malloc y asigna lo que le metamos en un buffer, luego ese buffer se lo asigna a la direccion de memoria del puntero, de esta manera el puntero sabe donde apuntar, a la direccion de memoria que tiene dentro, por eso es necesario el &, todo puntero creado apunta a una direccion de memoria aunque sea nulo, todo puntero apunta a la dirección de memoria que tiene dentro. Funciona con ms y con mc  

## \#include\<stdbool.h\>  
- Nos permite trabajar con booleanos  

## \#include\<stdlib.h\>  
- Nos permite trabajar con system() y usar EXIT\_SUCCESS  

## \#include\<stdint.h>
- Nos permite usar variables de tipo entero con capacidad de 64 bits, es decir, 8 bytes  
    - Se definen asi: uint64\_t  
- 1UL ?  
