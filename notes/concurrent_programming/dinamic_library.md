# Step 1
    Compilar los ficheros con la opcion -fPIC, creando asi los ficheros objeto preparados para ser utilizados y añadidos a una libreria dinamica
    g++ -fPIC -c main.cpp ...

# Step 2
    Linkar con la opcion -shared los ficheros objeto creados anteriormente para asi contruir la libreria compartida
    g++ -shared -o libreria.so fichero.o ...

# Step 3
    Si el programa es el mismo con el que compilamos en step 1 este paso no es necesario (ya esta echo), si no debemos compilar el programa y linkarlo con la libreria
    g++ -c programa.cpp
    g++ -o programa programa.o libreria.so

# Step 4
    Al linkar debemos de vincular la ruta de la libreria junto con el ejecutable, esto se puede hacer de dos maneras:
        - Modificando el valor de la variable de enorno LD\_LIBRARY\_PATH indicandole la ruta de nuestra libreria
        - Linkar indicando con la opcion -Wl,-rpath,/ruta_libreria
        - O por ultimo podemos copiar la libreria en /usr/lib
