# Que es un proceso y un Servicio
    Un proceso es un programa en ejecuciuon, este proceso se puede dividir en varios hilos (subtareas) pero nunca en otros procesos (Firefox --> Hilos/Pestañas Procesos/Ventanas). Cada proceso a diferencia de los hilos dispone de su propia pila de ejecucion, es decir, no comparten recursos (variables) con otros procesos.
Un servicio es un programa que no necesita tener una comunicaion activa con el usuario u otros disposivos, es decir que se ejecuta en segundo plano.  

# Paradigmas
Hay tres tipos de paradigmas:
- Paralelismo: Dos tareas se ejecutan en el mismo intervalo de tiempo, se puede dividir en tes tipos:
    - paralelismo simnulado --> paralelismo ejecutado por los hilos, dos tareas se ejecutan rapidamente una y otra vez pareciendo que hay paralelismo, siendo en realidad simulado
    - paralelismo real --> paralelismo ejecutado por los núcleos, aqui si existe paralelismo, varias tareas se jecutan al mismo tiempo, ya que son independientes en cada núcleo
    - paralelismo secuencial --> ejecucion basica, una tarea se ejecuta tras completar la anterior
- Solapamiento: Dos tareas se ejecutan en periodos de tiempo superpuestos, al mismo tiempo. El Paralelismo Real siempre implica solapàmiento, sin embarho el solapamiento no implica paralelismo real.
- Simultaneidad: Dos tareas se ejecutan en el mismo istante (inexistente)

# Relaciones entre Procesos
- Independencia: Los procesos son independientes unos de otros
- Competencia: Los procesos compiten entre ellos con el fin de coger/robar los recursos en su propio veneficio (obtener el minimo camino, sobrevivir)
- Cooperación: Los procesos cooperan entre ellos con el fin de resolver un problema, trabajan en distintas partes del programa

# Interacción entre procesos
- Sincronización: Los procesos intrercambian información sobre el flujo de ejecución, hay dos tipos:
    - Sincronización Condicional: Se espera hasta que se cumpla una condición para proseguir con la ejecución del programa. Provoca un Interbloqueo Pasivo, cede el paso. "Pasa tu, espero que pases tu y luego paso yo, si no pasa no paso"
    - Exclusión Mutua: Se compite por un recursos comun que tiene un acceso exclusivo. Provoca un interbloqueo Activo, retiene el paso. "Cojo el recurso y paso yo, hasta que no lo deje no pasas"
- Comunicación: Los procesos intercambian información sobre los datos que manejan/asociados a ellos

# Estados Basicos de un Proceso
- Preparado: Proceso preparado o en preparación para su ejecución.
- Ejecución: Proceso en ejecución, puede cambiar al estado preparado y volver a su estado en ejecución.
- Bloqueado: Proceso bloqueado sin opcion a cambiar de estado.  

    PREPARADO <-----> EJECUCION  
	    |                 |
	    |                 |
	    |                 |
	     --> BLOQUEADO <--

# Gestión de Procesos
- Planificación: Encargado de asignar los procesos al procesador en cada instante.
- Despecho: Encargado de entregar el control de la CPU al procerso correspondiente (Ejecución).
    
     Ambos lo lleva a cabo El Sistema de Soporte en Tiempo de Ejecución (SSTE)

# Solicitud de Ejecución
Podemos solicitar que un proceso se ejecute en dos formas:
    - Petición continua: Se llama a la ejecución del proceso y se ejecuta sin parar hasta finalizar. 
        PROCESS
            START -------> END
    - Petición Infinitamente Frecuente: Se llama a la ejecución del procerso cada cierto tiempo.
        PROCESS
            START - - - - - END

# Poriedad de Seguridad y Vida
- Las propiedades de seguridad siempre deben cumplirse, deben ser ciertas en cada instante.
    - La Sincronizació Condicional debe tener tener propiedades de seguridad siemrpre, ya que esta garantiza la ausencia de interbloqueo Pasivo (deadlok). Si mi programa tiene ausencia de interbloqueo pasivo se dice que he implementado propiedades de seguridad.
- Las propiedades de vida deben ejecutarse como minimo una vez, deben ser eventualmente ciertas.
    - La exclusion mutua debe tener propiedades de vida, ya que esta garantiza la ausencia de interbloqueo activo (livelock).

# Propiedades de Justicia
- Si se atiende una peticion continua se dice que se implemente una justicia debil (siemrpre veo la petición)
- Si se atiende una peticion infinitamente Frecuente se dice que se implementa una justicia fuerte (puedo no ver la petición nunca)

- Ausencia de Inanición: Garantiza que un proceso sea atentido por el micro

# Instrucciones Atomicas
Las instrucciones atomicas son aquellas que se garantizan indivisibles, son instrucciones que no se pueden dividir, se ejecutan hasta terminar
    - Grano Fino: Instrucciones que no se pueden dividir por Hardware, por ejemplo, si el micro esta ejecutando un proceso no puede ejecutar otro, si tengo el raton en la mano no puede tener una botella
    - Grano Grueso: Instrucciones que no se pueden dividir por Software, no poruqe no se pueda sino porque no se hace

