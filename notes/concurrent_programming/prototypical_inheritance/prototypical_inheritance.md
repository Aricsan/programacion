# Formas de utilizar un objeto como constructor
- B.prototype = new A
 ![alt text] (https://gitlab.com/Aricsan/programacion/-/tree/master/notes/concurrent_programming/out.png)
Creamos una copia de los atributos de A en el prototipo de B (no se pueden modificar)

- B.prototype = new A()
![alt text] (https://gitlab.com/Aricsan/programacion/-/tree/master/notes/concurrent_programming/out2.png)
Accedemos a los atributos de A mediante el prototipo de B (pudiendo modificarlas)
