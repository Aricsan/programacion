function A() {
	this.ruedas = 5
}

A.cables = 8 // los objetos de A no conocen esta variable, es una variable de clase, por lo que los objetos de B si la conocen ¿donde esta A.cables, como acceden los objetos de B a ella y porque pueden?

function B() {
	this.puertas = 2

	var parr2 = document.getElementById('parr2')
	parr2.innerHTML += "B puertas = " + puertas
}

B.prototype = new A // si lo defino debajo, B.prototype.puertas_metal desaparece
B.prototype.puertas_metal = 1

function c() {
	var parr1 = document.getElementById('parr1')

	var c = new B() // es indiferente poner () o no, c hace una copia de los atributos de B

	// Pruebas con el atributo del objeto B

	parr1.innerHTML += "c.puertas sin modificar = " + c.puertas + "<br>"
	c.puertas = 3 // modifica el atributo de c, ya que this apunta al objeto c
	this.c.puertas = 9 // ¿que hace?
	parr1.innerHTML += "c.puertas=3 -> " + c.puertas + "<br>"
	parr1.innerHTML += "B.puertas = " + puertas + "<br>"
	this.puertas = 4 // modifica el atributo de B, poruqe this aui apunta a B
	parr1.innerHTML += "this.puertas = 4 -> c.puertas = " + c.puertas + "<br>"
	parr1.innerHTML += "B.puertas = " + puertas + "<br> <br>"

	// Pruebas con los atributos del prototipo de B

	parr1.innerHTML += "c.puertas_metal sin modificar = " + c.puertas_metal + "<br>"
	c.puertas_metal = 2
	parr1.innerHTML += "c.puertas_metal=2 -> " + c.puertas_metal + "<br>"
	parr1.innerHTML += "B.prototype.puertas_metal = " + B.prototype.puertas_metal + "<br>"
	this.B.prototype.puertas_metal = 3
	parr1.innerHTML += "this.B.prototype.puertas_metal = 3 -> c.puertas_metal " + c.puertas_metal + "<br>"
	parr1.innerHTML += "this.B.prototype.puertas_metal -> " + B.prototype.puertas_metal + "<br> <br>"


	parr1.innerHTML += "c.ruedas sin modificar = " + c.ruedas + "<br>"
	c.ruedas = 8
	parr1.innerHTML += "c.ruedas=8 -> " + c.ruedas + "<br>"
	parr1.innerHTML += "B.prototype.ruedas = " + B.prototype.ruedas + "<br>"
	this.B.prototype.ruedas = 7
	parr1.innerHTML += "this.B.prototype.ruedas = 7 -> c.ruedas -> " + c.ruedas + "<br>"
	parr1.innerHTML += "this.B.prototype.ruedas -> " + B.prototype.ruedas + "<br>"
}
