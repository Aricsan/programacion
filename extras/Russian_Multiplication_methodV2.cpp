#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    int multiplicando, multiplicador, resultado = 0;

    printf ("Introduce el multiplicando => ");
    scanf (" %i", &multiplicando);
    printf ("Introduce el multiplicador => ");
    scanf (" %i", &multiplicador);

    do{
        if (!(multiplicando % 2 == 0))
            resultado += multiplicador;
        
        multiplicando /= 2;
        multiplicador *= 2;
    
    }while (multiplicando != 0);
    
    printf ("resultado => %i\n", resultado);

    return EXIT_SUCCESS;
}
