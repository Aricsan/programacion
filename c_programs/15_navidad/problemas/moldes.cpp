#include <stdio.h>
#include <stdlib.h>

int main () {
    
    double a = 13.0;
    int b = 7;

    printf ("El resto de la division %.1lf / %i es: %i\n", a, b, (int) a%b);
    return EXIT_SUCCESS;
}
