#include <stdio.h>
#include <stdio_ext.h>
 
 unsigned char intensidad(const char *color) {
     unsigned int mascara;
 
     do {
         __fpurge (stdin);
         printf ("Introduce la intensidad del color %s con un valor entre [0-255]: ", color);
         scanf (" %u", &mascara);
     } while (mascara > 0xFF);
     
     return (unsigned char) mascara;
 }   
 
 int main () {
 
     unsigned char r, g, b;
     unsigned char xr;
 
     r = intensidad ("Rojo");
     g = intensidad ("Verde");
     b = intensidad ("Azul");
 
     xr = r ^ 0xFF; // 255
     
     printf ("%i %i %i => %i %i %i\n", r, g, b, xr, g, b);
     
     
     return 0;
}    

