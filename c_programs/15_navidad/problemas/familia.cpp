#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    mama,
    hijo,
    hija,
    total_miembros
};

/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {
    int bit;
    switch (miembro) {
        /* Rellena todos los casos */
        case papa:
             bit = PAPUP;
             break;

        case mama:
             bit = MAMUP;
             break;
        
        case hijo:
             bit = HIJOP;
             break;
        
        case hija:
             bit = HIJAP;
         /* Realiza la operación a nivel de bits necesaria para
           cambiar el bit correspondiente. Usa los valores en los
           define. No uses if. */
            break;

        default:
            bit = 0;
    }
    old_status ^= bit;
    return old_status;
}

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    int bit = 1, //pongo un 1 porque asi me puedo ahorar una ejecucion del for, si fuese 0 como minimo entraria una vez
        posicion = total_miembros - 1 - miembro;//si pongo el bit a 1 tengo que restarle 1 al total de mienbros, porque sino podria contar uno de mas, me saldria
    for (int i = 0; i < posicion; i++)
        bit <<= 1;
    return old_status & ~bit;
}

/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
    int posicion = total_miembros - 1 - levantado;

    return old_status | (int) pow (2, posicion);
}

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá está %s\n", waked & PAPUP/* Usa una operación de bits*/ ? "levantado": "acostado");
    printf ("Mama está %s\n", waked & MAMUP/* Usa una operación de bits*/ ? "levantado": "acostado");
    printf ("Hijo está %s\n", waked & HIJOP/* Usa una operación de bits*/ ? "levantado": "acostado");
    printf ("Hija está %s\n", waked & HIJAP/* Usa una operación de bits*/ ? "levantado": "acostado");
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa;
    
    waked = levantar (waked, papa);
    waked = acostar (waked, papa);
    waked = conmutar (waked, papa);
    
    print (waked);
    
    return EXIT_SUCCESS;
}
