#include <stdio.h>
#include <math.h>
#include <stdio_ext.h>

#include "interfaz.h"

void coo_cilindricas (double x, double y, double z){
        y = atan2 (y,x) * 180 / M_PI;
        x = sqrt (pow (x,2) + pow (y,2));

    printf ("\nEl vector introducido es (%.2lf, %.2lf, %.2lf) en coordenadas cilindricas\n", x, y, z);
}    
    
void coo_cartesianas (double x, double y, double z){
    double r;
    
    r = sqrt (pow (x,2) + pow (y,2));
    x = r * cos (x/r);
    y = r * sin (y/r);
    
    printf ("\nEl vector introducido es (%.2lf, %.2lf, %.2lf) en coordenadas cartesianas\n", x, y, z);
}   
        
int coordenadas (){
    int eleccion;
 
    do{
        printf ("¿En que coordenadas definiras el vector?\n\
        1- Cartesianas\n\
        2- Cilindricas\n");
        scanf ( "%i", &eleccion);
    
    }while (eleccion != 1 && eleccion != 2);

    __fpurge (stdin);

    return eleccion;
}
