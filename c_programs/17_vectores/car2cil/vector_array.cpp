#include <stdio.h>
#include <stdlib.h>

#include "common.h"
#include "interfaz.h"

int main (int argc, char *argv[]) {
    
    unsigned d = 0, i = 0, elec = 0;
    double *v;
        
    elec = coordenadas();    
    printf ("Introduce un vector\n");
    
    do{
        v = (double *) realloc (v, (i+1) * sizeof (double));
        d += scanf ("%*[]()[, ] %lf %*[])]", &v[i++]); 

    } while ((getchar()) != '\n');
    
    if (elec == 1)
        coo_cilindricas (v[X], v[Y], v[Z]);
    
    if (elec == 2)
        coo_cartesianas (v[R], v[T], v[Z]);
    
    printf ("El vector tiene una dimension de %i\n", d);

    free(v);
   
    return EXIT_SUCCESS;
}
