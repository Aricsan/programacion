#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

#ifdef __cplusplus
extern "C"{
#endif

void coo_cilindricas (double x, double y, double z);
void coo_cartesianas (double x, double y, double z);
int coordenadas ();

#ifdef __cplusplus
}
#endif

#endif
