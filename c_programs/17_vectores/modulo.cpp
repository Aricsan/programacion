#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int modulo (int a, int b, int c){
    int modulo;
    
    modulo = sqrt (pow (a,2) + pow (b,2) + pow(c,2));
    
    return modulo;
}

int main (int argc, char *argv[]) {
    
    int a, b, c;

    printf ("Inserte el vector\n");
    scanf (" %*[([] %i %*[, ] %i %*[, ] %i %*[])]", &a, &b, &c);

    printf ("El modulo del vector introducido es => %i\n", modulo (a, b, c));
    
    return EXIT_SUCCESS;
}
