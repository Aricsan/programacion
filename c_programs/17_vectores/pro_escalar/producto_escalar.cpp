#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double mul_x = 1, mul_y = 1, mul_z = 1;
double sum_modulos = 0, res_mod, res_mul, res_escalar;

void mul_vectores (double x, double y, double z){
    mul_x *= x * 1;
    mul_y *= y * 1;
    mul_z *= z * 1;
    res_mul = mul_x + mul_y + mul_z;
}

void mul_modulos (double x, double y, double z){
    sum_modulos += pow(x,2) + pow(y,2) + pow (z,2);
    res_mod = sqrt (sum_modulos);
}

void producto_escalar (double numerador, double denominador){
    res_escalar = numerador / denominador;
    res_escalar = acos (res_escalar);
}

void rad2gra (double rad){
    res_escalar = (res_escalar * 180) / M_PI;
}

int main (int argc, char *argv[]) {
    
    double x, y, z;
    int n_vectores;

    printf ("Introduce la cantidad de vectores ");
    scanf ("%i", &n_vectores);
    
    for (int i=0; i<n_vectores; i++){
        printf ("Introduce un vector\n");
        scanf (" %*[([] %lf %*[, ] %lf %*[, ] %lf %*[])]", &x, &y, &z);
        mul_vectores (x, y, z);
        mul_modulos (x, y, z);
    }

    producto_escalar(res_mul, res_mod);
    rad2gra(res_escalar);

    printf ("El producto escalar de los vectores introducidos es => %gº\n", res_escalar);
    
    return EXIT_SUCCESS;
}
