#include <stdio.h>
#include <stdlib.h>
#define PICAS_BLACK "\u2660"
#define PICAS_WHITE "\u2664"
#define TREBOLES_BLACK "\u2663"
#define TREBOLES_WHITE "\u2667"
#define ROMBOS_BLACK "\u2666"
#define ROMBOS_WHITE "\u2662"
#define CORAZONES_BLACK "\u2665"
#define CORAZONES_WHITE "\u2661"

int main (int argc, char *argv[]) {
    int base;
    int lado = 0;
    char caracter[5];

    printf ("Introduzca la base que desee: ");
    scanf ("%i", &base);    
    
    for(int i = 0; i <= base; i++)
        printf ("*");
    
    printf ("\n\nIntroduzca la longitud del lado del cuadrado: ");
    scanf (" %i", &lado);
    printf ("Introduzca el caracter: ");
    scanf (" %s", caracter);

    for (int i = 1; i <= lado; i++){
        printf ("%s", caracter);
        for (int j = 1; j < lado; j++)
            printf ("%s", caracter);
        printf("\n");
    }
    
    printf ("\nTriangulo Isosceles\n");

    for (int i = 0; i <= base; i++){
        for (unsigned int j = 0; j < i; j++)
             printf ("%s", caracter);
        
        printf ("\n");
    }

    printf("\nUnicodes del palo corazones\n%s %s\nUnicodes del palo picas\n%s %s\nUnicodes del palo treboles\n%s %s\nUnicodes del palo rombos\n%s %s\n", CORAZONES_BLACK, CORAZONES_WHITE, PICAS_BLACK, PICAS_WHITE, TREBOLES_BLACK, TREBOLES_WHITE, ROMBOS_BLACK, ROMBOS_WHITE);


    return EXIT_SUCCESS;
}
