
#include <stdio.h>
#include <stdlib.h>
//const char *carta = "";//el astelisco se pone porque carta almacena la direccion de memoria del principio de una cadena de caracteres y para mostrar la cadena tenemos que indicar que queremos saber lo que contiene la direccion de memoria de carta, el const se pone para no sobreescribir el programa

int main (int argc, char *argv[]) {
    char carta[] = "♠";//si no especificamos cuanto de grande es el char lo hace automaticamente, es este caso sera de 5 bytes, 4 de caracteres y 1 por el 0
    char *fin = carta +3;//esto recorre la cadena de caracteres de la carta, para llegar al final e incrementar el ultimo caracter en 1, de ese modo conseguimos la siguiente carta
    char *fin;
    fin = carta +3;
    ++(*fin); //incremento lo que contiene carta
    //notacion de matrices
    carta[3]++;
    printf ("%s\n", carta);
    
    ++(*(carta + 3)); //carta vale 6e3 (direccion del primer caracter de la cadena) sumale 3, que tiene 6e6 (direccion del ultimo caracter de la cadena) me lo incrementas lo que tenga 6e6, esto es lo mismo que lo de arriba

    //hacerlo en un entero
    int carta = 0xA1828CF0;
    carta += 0x01000000;
    printf ("%s\n", (char *) &carta); //carta tiene la cadena de caracteres de la carta, al imprimirlo tenemos que indicar que queremos imprimir la direccion de carta, y ponemos char * para que al incrementarse se incremente de byte en byte no de 4 bytes en 4 bytes

    return EXIT_SUCCESS;
}
