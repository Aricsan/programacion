#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    unsigned int altura;
    char caracter [5];
    
    printf ("Untroduce la altura del triangulo: ");
    scanf (" %i", &altura);
    printf ("Introduce el caracter: ");
    scanf (" %s", caracter);

    printf ("\nTriangulo Hueco\n");

    for (int f = 0; f <= altura; f++){
        for (int c = 0; c < f; c++){
            if (c == 0 || c == f -1
                || f == altura)
            printf ("%s", caracter);
            else{
                printf (" ");
            }
        }
        printf ("\n");
    }

    printf ("\nTriangulo girado\n");
    
    for (int f = 0; f <= altura; f++){
        for (int c = 0; c < altura; c++){
            if (c >= altura -f)
                printf ("%s", caracter);
            else{
                printf (" ");
            }
        }
        printf ("\n");
    }

    printf ("\nTriangulo girado hueco\n\n");
    
    for (int f = 1; f <= altura; f++){
        for (int c = 0; c < altura; c++){
            if (c == altura -1 || c == altura -f || f == altura)
                printf ("%s", caracter);
            else{
                printf (" ");
             }
         }
        printf ("\n");
    }
   
    printf ("\nArbol\n");
    
    for (int i = 0; i < 3; i++){
        for (int f = 0; f <= altura; f++){
            for (int c = 0; c <= f; c++){
                if (c >= altura -f || f == altura)
                    printf ("%s", caracter);
                else{
                    printf (" ");
            }
        }
        printf ("\n");
    }
}
    /*edificio
    for (int f = 0; f <= altura; f++){
        for (int c = 0; c <= altura; c++){ 
            for (int j = 0; j < f; j++){
            if (j >= altura -f){
                printf ("%s", caracter);
            }
            else{
                printf (" ");
            }
        }
        printf ("\n");
    }
}*/
  
return EXIT_SUCCESS;
}
