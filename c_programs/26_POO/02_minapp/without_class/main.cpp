#include <stdlib.h>

#include "persona.h"

int main (int argc, char *argv[]) {
    
    TPersona persona;

    init_per (&persona, "Alejrando", "Garcia", 30);
    mostrar (&persona);

    return EXIT_SUCCESS;
}
