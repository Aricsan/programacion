#ifndef __PERSONA_H__
#define __PERSONA_H__

#define MAX 0x50

struct TPersona {
    char nombre[MAX];
    char apellidos[MAX];
    unsigned edad;

};
/* C exported functions */
#ifdef __cplusplus
extern "C"
{
#endif

void init_per (struct TPersona *p, const char *nombre, const char *apellidos, unsigned edad);
void mostrar (struct TPersona *p);

#ifdef __cplusplus
}
#endif

#endif
