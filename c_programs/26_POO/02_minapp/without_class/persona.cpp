#include <string.h>
#include <stdio.h>

#include "persona.h"

void init_per (struct TPersona *p, const char *nombre, const char *apellidos, unsigned edad){
    
    strcpy (p->nombre, nombre);
    strcpy (p->apellidos, apellidos);
    p->edad = edad;
}

void mostrar (struct TPersona *p){
    printf ("\tNombre --> %s\n\tApellidos --> %s\n", p->nombre, p->apellidos);
    printf ("\tEdad --> %u\n", p->edad);
}
