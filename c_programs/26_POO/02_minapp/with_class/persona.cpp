#include <stdio.h>
#include <string.h>

#include "cpersona.h"


CPersona::CPersona (const char *nombre, const char *apellidos, unsigned edad){ //llamando al constructor
    strcpy (this->nombre, nombre);
    strcpy (this->apellidos, apellidos);
    this->edad = edad;
}

void CPersona::mostrar (){
    printf ("\tNombre --> %s\n\tApellidos --> %s\n", this->nombre, this->apellidos);
    printf ("\tEdad --> %u\n", this->edad);
}
