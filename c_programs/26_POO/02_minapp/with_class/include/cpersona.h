#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define MAX 0x50

class CPersona {
    char nombre[MAX];
    char apellidos[MAX];
    unsigned edad;
    
    public:
    
    CPersona () = delete; //contructor por defecto, le asignamos delete para borrarlo
    CPersona (const char *nombre, const char *apellidos, unsigned edad);
    void mostrar ();
};

/* C exported functions */
#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __cplusplus
}
#endif

#endif
