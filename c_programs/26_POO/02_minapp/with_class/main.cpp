#include <stdio.h>
#include <stdlib.h>

#include "cpersona.h"

int main (int argc, char *argv[]) {

    CPersona persona ("Juan", "Garcia", 50);
    
    persona.mostrar ();

    return EXIT_SUCCESS;
}
