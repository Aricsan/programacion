#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define MAX 0x50

class Persona {

    public:
        Persona (char *nombre);
        
        void MostrarNombre (char *nombre);

    protected:
        char nombre[40];
};

// Clase que hereda de Persona
class Empleado : public Persona {
    
    public:
        Empleado (char *nombre, double salario);
        
        double MostrarSalario (double salario);
        double salario;
};

/* C exported functions */
#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __cplusplus
}
#endif

#endif

