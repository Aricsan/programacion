#include <stdio.h>
#include <stdlib.h>

#include "persona.h"

int main (int argc, char *argv[]) {

    Persona pepe ("Pepe");
    Empleado juan ("Juan", 1654.123);
    
    pepe.MostrarNombre ("Pepe");
    juan.MostrarNombre ("Juan");
    juan.MostrarSalario (1650.124);
    
    return EXIT_SUCCESS;
}
