#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define MAX 0x50

class CPersona {
    char nombre[MAX];
    
    public:
    void saluda ();
    CPersona (char nombre[MAX]);
};

#endif
