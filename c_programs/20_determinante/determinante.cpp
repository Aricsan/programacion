#include <stdio.h>
#include <stdlib.h>

#define D 3

double determinante_pos (double matriz[D][D]){
    
    double producto0 = matriz[0][0], producto1 = matriz[1][0], producto2 = matriz[2][0];
    double resultado;

    for (int i=0; i+1<=2; i++){
        producto0 *= matriz[i+1][i+1]; 
        producto1 *= matriz[(i+2)%D][(i+1)%D]; 
        producto2 *= matriz[(i+3)%D][(i+1)%D]; 
    }
    
    resultado = producto0 + producto1 + producto2;
    producto0 = matriz[0][2];
    producto1 = matriz[0][1];
    producto2 = matriz[0][0];

    for (int i=2, j=0; j+1<=2; i--, j++){
        producto0 *= matriz[j+1][i-1];
        producto1 *= matriz[j+1][(i+1)%3]; 
        producto2 *= matriz[j+1][i];
    }

    return resultado -= producto0 + producto1 + producto2;
}

int main (int argc, char *argv[]) {

    double determinante, matriz[D][D] = {
        {3, 5, 9},
        {7, 2, 4},
        {5, 8, 5}};

    determinante = determinante_pos (matriz);
    
    printf ("El determinante es: %g\n", determinante);

    return EXIT_SUCCESS;
}
