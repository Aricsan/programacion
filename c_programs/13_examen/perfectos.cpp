#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    unsigned perfecto = 0;
    unsigned total = 0;
    
    for (int i=1; total != 5; i++, perfecto = 0){
        for (int j=i-1; j>=1; j--)
            if (i % j == 0)
                perfecto += j;        
            
        if (perfecto == i){
            printf ("%u\n", i);
            total++;
        }    
    }

    return EXIT_SUCCESS;
}
