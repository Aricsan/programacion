#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    double matriz[3][3];
    double resultado[3][3];

    printf ("Introduce los valores de los vectores:\n");
    
    for (int f=0; f<3; f++)
        for (int c=0; c<3; c++)
            scanf (" %lf", &matriz[f][c]);
    
    // Producto escalar filas
    for (int f=0; f<3; f++)
        for (int c=0; c<3; c++){
            if (f == 2)
                resultado[f][c] = matriz[f][c] * matriz[0][c]; 
            else
                resultado[f][c] = matriz[f][c] * matriz[f+1][c];
        }
    
    printf ("\nEl producto escalar de las filas es:\n");
    for (int f=0; f<3; f++){
        for (int c=0; c<3; c++)
            printf ("%5g ", resultado[f][c]);
        
        printf ("\n");
    }
    
    // Producto escalar columnas
    for (int c=0; c<3; c++)
        for (int f=0; f<3; f++){
            if (c == 2)
                resultado[f][c] = matriz[f][2] * matriz[f][0]; 
            else
                resultado[f][c] = matriz[f][c] * matriz[f][c+1];
        }
    
    printf ("\nEl producto escalar de las columnas es:\n");
    for (int f=0; f<3; f++){
        for (int c=0; c<3; c++)
            printf ("%5g ", resultado[f][c]);
        
        printf ("\n");
    }
   
   // Producto escalar diagonal
    for (int f=0, c=2; f<3; f++, c--)
        resultado[f][f] = matriz[f][f] * matriz[f][c];
    
    printf ("\nEl producto escalar de las diagonales es:\n");
    for (int f=0; f<3; f++){
        for (int c=0; c<3; c++)
            printf ("%5g ", resultado[f][c]);
        
        printf ("\n");
    }
            
    return EXIT_SUCCESS;
}
