#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    int rojo, verde, azul;

    printf ("Si el color rojo esta activado introduza 1 por el contrario introcuzca 0 => ");
        scanf (" %i", &rojo);
    printf ("Si el color verde esta activado introduza 1 por el contrario introcuzca 0 => ");
        scanf (" %i", &verde);
    printf ("Si el color azul esta activado introduza 1 por el contrario introcuzca 0 => ");
        scanf (" %i", &azul);

    if (rojo && verde && azul)
        printf ("EL color combinado es el: Blanco\n");
    
    if (!rojo && !verde && !azul) 
        printf ("EL color combinado es el: Negro\n");
    
    if (rojo && !verde && !azul)
        printf ("EL color combinado es el: Rojo\n");
    
    if (verde && !rojo && !azul)
        printf ("EL color combinado es el: Verde\n");

    if (azul && !rojo && !verde)
        printf ("EL color combinado es el: Azul\n");

    if (rojo && verde && !azul)
        printf ("EL color combinado es el: Amarillo\n");

    if (verde && azul && !rojo)
        printf ("EL color combinado es el: Cian\n");

    if (rojo && azul && !verde)
        printf ("EL color combinado es el: Magenta\n");

    return EXIT_SUCCESS;
}
