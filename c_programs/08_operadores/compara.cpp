#include <stdio.h>
#include <stdlib.h>

#define MAX_ERROR .00000000001
int main (int argc, char *argv[]) {

    double user_number;
   
    printf ("NUmber: ");
    scanf (" %lf", &user_number);

    if (user_number >= 3. - MAX_ERROR &&
        user_number <= 3. + MAX_ERROR)
        printf ("Para mi es un 3 a todos los efectos\n");
    else
        printf ("Para mi no es un 3\n");
    return EXIT_SUCCESS;
}
