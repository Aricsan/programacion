#include "fbd.h" //si la libreria es tuya entre comillas, si es del sistema entre <>

void circle () { //decir que va a haber en esa direccion de memoria, void es que devuelve algo vacio
    for (double a=0; a<2*M_PI; a +=.001)
    put (R*cos(a), R*sin(a), 0xFF, 0xFF, 0xFF, 0xFF);
} 

int main () {
    
    open_fb ();   //iniciar la libreria
    put (500, 350, 0xFF, 0xFF, 0xFF, 0xFF);
    circle (); //nomnre y posicion de memoria es lo mismo, los nombres se llaman identificadores. Cuando le ponemos unos parentesis decimos que ejecute esa direccion de memoria
    close:fb (); //finalizar la libreria
    
    return 0;
}

