#include <stdio.h>
#include <stdbool.h>
int main (){
	
	int multiplicador = 0, multiplicando = 0, resultado = 0, resto = 0;
	bool bucle = true;
	
	printf("\n---BIENVENIDOS AL METODO DE MULTIPLICACION RUSA---\n\n Antes de empezar una breve introduccion:\n\n ");
	printf("El metodo de multiplicacion rusa consiste en multiplicar sucesivamente por 2 el multiplicando y dividir por 2 el\n multiplicador hasta que el multiplicador tome el valor 1. Luego, se suman todos los multiplicandos correspondientes\n a los multiplicadores impares. Dicha suma es el producto de los dos numeros\n\n");
	printf(" Introduce el numero multiplicador: ");
	scanf("%i",&multiplicador);
	printf(" Introduce el numero multiplicando: ");
	scanf("%i",&multiplicando);
	printf("\n %i * %i", multiplicador, multiplicando);
	printf("\n\n /2   *2\n");	
	int i;
	for (i = 0; bucle == true; i++){				
		resto = multiplicador % 2;
		
		if (multiplicador == 1){
			bucle = false;
		}	
					
		if (resto == 1){
			resultado += multiplicando;
		}
		
		printf(" %i | ", multiplicador);
		printf("%i\n", multiplicando);
		multiplicador = multiplicador / 2;
		multiplicando = multiplicando *2;
						
	}
			
		printf("\n El resultado es: %i\n", resultado);
				
	return 0;
}
