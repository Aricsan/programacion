#include <stdio.h>
int main (){
    
    long int long_int;
    short int short_int;
    long long int long_long_int;
    int normal_int;
    double normal_double;
    long double long_double;
    long long long_long_double;
    //short double short_double; no existe porque sino seria un entero
    char normal_char;
    //long char long_char;
    //short char short_char;
   
    printf ("long int: %lu bytes.\n", sizeof (long_int));//%lu (muestra el resultado de la 
    //siguiente operacion) sizeof (muestrame el tamaño  de la variable (x))
    printf ("short int: %lu bytes.\n", sizeof (short_int));
    printf ("long long int: %lu bytes.\n", sizeof (long_long_int));
    printf ("normal int: %lu bytes.\n", sizeof (normal_int));
    printf ("normal double: %lu bytes.\n", sizeof (normal_double));
    printf ("long double: %lu bytes.\n", sizeof (long_double));
    printf ("long long double: %lu bytes.\n", sizeof (long_long_double));
    printf ("char: %lu bytes.\n", sizeof (normal_char));
    //printf ("long char: %lu bytes.\n", sizeof (long_char));
    //printf ("short char: %lu bytes.\n", sizeof (short_char));
    return 0;
}
