#include <stdio.h>
#include <stdlib.h>

int main () {
    
    int numero;

    do{
void __fpurge(FILE *stream);

        printf ("Introduce un numero del 1 al 10: ");
            scanf (" %i", &numero);
    }
    while (numero == 0 || numero > 10);
        printf ("El numero introducido es el siguiente: %i\n", numero);

    return EXIT_SUCCESS;
}
