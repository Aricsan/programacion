#include <stdio.h>
#include <stdlib.h>
#define MAX 10

int main () {
    
    char nombre[MAX];
    int veces;

    printf ("Introduce un nombre, maximo 10 caracteres: ");
    fgets (nombre, MAX, stdin);
    printf ("Introduce las veces que se repetira el nombre: ");
    scanf (" %i", &veces);

    for(int i = 0; i < veces; i++)
        printf ("%s", nombre);
    return EXIT_SUCCESS;
}
