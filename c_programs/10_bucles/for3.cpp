#include <stdio.h>
#include <stdlib.h>

int main () {
    
    char *nombre;
    int veces;

    printf ("Introduce un nombre: ");
    scanf (" %s", *(&nombre));
    printf ("Introduce la cantidad de veces que se repetira el nombre: ");
    scanf (" %i", &veces);
    
    for(int i = 0; i < veces; i++)
        printf ("%s\n", nombre);
    return EXIT_SUCCESS;
}
