#include <stdio.h>
#include <stdlib.h>

int main () {
    
    printf ("FIGURA 1\n\n");

    for (int f = 0; f < 5; f++){
        for (int c = 0; c < 8; c++)
            if (f == 0 || f == 4)
                printf ("*");
            else 
                printf (" ");
    printf ("\n");
    }
    
    printf ("\nFIGURA 2\n\n");

    for (int f = 0; f < 5; f++){
        for (int c = 0; c < 8; c++)
            if (c == 0 || c == 7)
                printf ("*");
            else 
                printf (" ");
    printf ("\n");
    }

    printf ("\nFIGURA 3\n\n");

    for (int f = 0; f < 5; f++){
        for (int c = 0; c < 8; c++)
            if (c == 0 || c == 7
               || f == 0 || f == 4)
                printf ("*");
            else 
                printf (" ");
    printf ("\n");
    }

    printf ("\nFIGURA 5\n\n");

    for (int f = 0; f < 8; f++){
        for (int c = 0; c <= f; c++)
            if (c == f)
                printf ("*");
            else 
                printf (" ");
    printf ("\n");
    }
    
    printf ("\nFIGURA 6\n\n");

    for (int f = 0; f < 8; f++){
        for (int c = 0; c < 8; c++){
            if (c == (7 - f))
                printf ("*");
            else
                printf (" ");
        }
    printf ("\n");
    }
    
    printf ("\nFIGURA 7\n\n");

    for (int f = 0; f < 8; f++){
        for (int c = 0; c < 8; c++)
            if (c == 0 || c == 7
               || f == 0 || f == 7
               || c == f || c == (7-f))
                printf ("*");
            else 
                printf (" ");
    printf ("\n");
    }


    return EXIT_SUCCESS;
}
