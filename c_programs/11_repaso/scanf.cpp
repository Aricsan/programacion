#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

int main (int argc, char *argv[]) {

    char numeros[MAX];
    char letra;
    int i = 0;

    while( (letra = getchar()) != '\n'){
        ungetc ('5', stdin);
        scanf(" %1[0-9]", &numeros[i]);
        numeros[i] -= '0';
        printf ("%i ", numeros[i]);
        i++;
    }
    
    printf ("\n");
    
    return EXIT_SUCCESS;
}
