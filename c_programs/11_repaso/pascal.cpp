#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int main (int argc, char *argv[]) {
    
    int matriz[MAX][MAX];
    
    for(int f=0; f<MAX; f++){
        for(int c=0; c<MAX; c++){
            if(f == 0)
               matriz[c][f] = 0;
            
            if(c == 0)
               matriz[c][f] = 1;
            
            if(c > 0 && f > 0)
                matriz[c][f] = matriz[c][f-1] + matriz[c-1][f-1];
            
            printf("%3.i ", matriz[c][f]);
        }

        printf("\n");
    }
    
    printf("\n");
    
    return EXIT_SUCCESS;
}
