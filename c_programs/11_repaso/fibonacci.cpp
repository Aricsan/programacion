#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define MAX 0x100

int main (int argc, char *argv[]) {

    unsigned long int serie[MAX];
    serie[0] = 1;
    serie[1] = 1;
    int cont = 0; 
    
    /*long double numero = pow(2,64);
    unsigned long int pepe = numero-2;*/

    for(int i=2; i<MAX && serie[i-1]<pow(2,63); i++){
        cont++;
        serie[i] = serie[i-2] + serie[i-1];
        printf ("%lu\n", serie[i]);
    }
    
    //printf ("%Lf %i %li", numero-1, cont, pepe);

    return EXIT_SUCCESS;
}
