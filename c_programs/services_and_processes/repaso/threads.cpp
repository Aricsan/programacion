/*
 * =====================================================================================
 *
 *       Filename:  threads.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/11/20 19:26:05
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */

#include <pthread.h>
#include <stdio.h>

void* print_xs (void* unused){

    while (1)
        fputc ('x', stderr);

    return NULL;
}

int main (){

    pthread_t thread_id;
    pthread_create (&thread_id, NULL, &print_xs, NULL);

    while (1)
        fputc ('o', stderr);

    return 0;
}
