#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

//El nombre del programa
const char* program_name;

/* Imprime la información de uso de este programa en STREAM (normalmente
stdout o stderr) y la salida del programa con EXIT_CODE.*/

void print_usage (FILE* stream, int exit_code)
{
    fprintf (stream, "Usage: %s options [ inputfile ... ]\n", program_name);
    fprintf (stream, " -h --help Display this usage information.\n"\
                     " -o --output filename Write output to file.\n"\
                     " -v --verbose Print verbose messages.\n");
    
    exit (exit_code);
}

/* Punto de entrada del programa principal. argc contiene el número total de argumentos
 * argv un array de punteros a ellos */

int main (int argc, char* argv[])
{
    int next_option;

/* Una cadena que enumera letras de opciones cortas válidas*/
    const char* const short_options = "ho:v";

/* Una matriz que describe opciones largas válidas*/

    const struct option long_options[] = {
        { "help", 0, NULL, 'h' },
        { "output", 1, NULL, 'o' },
        { "verbose", 0, NULL, 'v' },
        { NULL, 0, NULL, 0},
    };

/* El nombre del archivo para recibir la salida del programa, o NULL para
salida estándar */
    const char* output_filename = NULL;

/* Mostrar mensajes detallados */
    int verbose = 0;

/* El nombre del programa se almacena en argv [0] */
    program_name = argv[0];
    
    do {
        next_option = getopt_long (argc, argv, short_options, long_options, NULL);
        
    switch (next_option) {
        case 'h':
            /* -h o --help */
            /* El usuario ha solicitado información de uso. Impríme mensaje con el código de salida cero (terminación normal) */
            print_usage (stdout, 0);
        
        case 'o':
            /* -o o --output */
            /* Esta opcion necesita un argumento (el nombre del fichero de salida) */
            output_filename = optarg;
            break;
        
        case 'v':
            /* -v or --verbose */
            verbose = 1;
            break;
        
        case '?':
            /* El usuario especificó una opción no válida.
             * Imprime la información de uso en stderr junto con el código uno 
             * (que indica una terminación anormal) */
            print_usage (stderr, 1);
        
        case -1: // No hay mas opciones
            break;
        
        default: // Opción inesperada
            abort ();
    }

}while (next_option != -1);

/* OPTIND apunta al primer argumento de ...
   Imprime si verbose esta a 1*/

    if (verbose) {
        for (int i = optind; i < argc; ++i)
            printf ("Argument: %s\n", argv[i]);
    }
    
    return 0;
}
