#include <stdio.h>
#include <unistd.h>

int main (){
    
    for (char **var = environ; *var != NULL; ++var)
        printf ("%s\n", *var);
    
    return 0;
}
