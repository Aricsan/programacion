/*
 * =====================================================================================
 *
 *       Filename:  fibo_fork.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  25/10/20 12:48:52
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

struct TParam {
    int firs_num;
    int stop;
    int *array;
};

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; // Forma de declaración recomendada para las variables globales
                                                   /* Forma normal, el segundo parametro son los atributos de creación
                                                    * ptthread_mutex_t mutex;
                                                    * pthread_mutex_init (&mutex, NULL);
                                                    */ 

void fibo (struct TParam *p){

    int number;
    
    // Preprotocolo
    pthread_mutex_lock (&mutex);
    
    // Sección critica
    for (int i = 1; p->array[i] < (p->firs_num)+1; i++){
        p->array = (int *) realloc (p->array, (i+2) * sizeof(int));
        p->array[i+1] = p->array[i] + p->array [i-1];
        number = p->array[i+1];
    }
    
    printf("\t-> %i\n", number);
    p->firs_num = number;
   
    // Posprotocolo
    pthread_mutex_unlock (&mutex);
}

void *spawn (void *parameters){
    
    int last_pos = 0;
    struct TParam *p = (struct TParam *) parameters;

    while (p->array[last_pos] < p->stop){
        fibo (p);
        for (int j = 1; p->array[j] != '\0'; j++){
            last_pos = j;
        }
    }
}

int main (int argc, char *argv[]) {

    struct TParam thread_arg;
    pthread_t thread_id1;
    pthread_t thread_id2;

    thread_arg.array = (int *) malloc (2 * sizeof(int));
    thread_arg.array[0] = 0;
    thread_arg.array[1] = 1;
    thread_arg.firs_num = atoi (argv[1]);
    thread_arg.stop = atoi (argv[2]);
    
    printf ("Los numeros entre %i y %i segun la sucesión de Fibonacci son:\n", thread_arg.firs_num, thread_arg.stop);
    
    pthread_create (&thread_id1, NULL, &spawn, &thread_arg);
    pthread_create (&thread_id2, NULL, &spawn, &thread_arg);
    
    pthread_join (thread_id1, NULL);
    pthread_join (thread_id2, NULL);

    free (thread_arg.array);

    return EXIT_SUCCESS;
}
