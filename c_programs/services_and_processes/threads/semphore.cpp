/*
 * =====================================================================================
 *
 *       Filename:  semphore.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  25/11/20 18:19:14
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>

int cont = 0;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t semaphore;

void *incrementar (void *parameters){
    
    pthread_mutex_lock (&mutex);

    if (cont < 10)
        cont++;
    else
        exit (0);
    
    sem_post (&semaphore);
    
    pthread_mutex_unlock (&mutex);

    printf ("incremento %i\n", cont);
}

void *decrementar (void *parameters){

    sem_wait (&semaphore);

    pthread_mutex_lock (&mutex);
    
    if (cont == 0)
        cont--;
    else
        exit (0);
    
    pthread_mutex_unlock (&mutex);

    printf ("decremento %i\n", cont);
}

int main (int argc, char *argv[]) {
    
    sem_init (&semaphore, 0, 0);

    // productores
    pthread_t thread_idp1;
    pthread_t thread_idp2;

    pthread_create (&thread_idp1, NULL, &incrementar, NULL);
    pthread_create (&thread_idp2, NULL, &incrementar, NULL);

    // consumidores
    pthread_t thread_idc1;
    pthread_t thread_idc2;

    pthread_create (&thread_idc1, NULL, &decrementar, NULL);
    //pthread_create (&thread_idc2, NULL, &decrementar, NULL);

    // esperar a terminar
    pthread_join (thread_idp1, NULL);
    pthread_join (thread_idp2, NULL);
    pthread_join (thread_idc1, NULL);
    pthread_join (thread_idc2, NULL);

    return EXIT_SUCCESS;
}

