/*
 * =====================================================================================
 *
 *       Filename:  signal_handler.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  29/10/20 19:30:59
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>

#define MAX 100

int sigusr_cont = 0;
int cont = 0;
int *stack;
int descriptor;

void read_file (int descriptor){
   
   int read_buffer[cont];

   read (descriptor, read_buffer, MAX);
   lseek (descriptor, 0, SEEK_SET);
   
   printf ("First 100 prime numbers\n");
   for (int i = 0; i < cont; i++)
       printf ("Number [%i] = %i\n", i+1, read_buffer[i]);

   close(descriptor);
}

void events (int signal_number){
    
    switch (signal_number){
        case SIGINT:
            printf ("\nThe use of Ctrl + c is not allowed\n");
            break;
        case SIGUSR1:
            printf ("\nNumber of SIGUSR1 signals received: %i\n", ++sigusr_cont);
            break;
        case SIGCHLD:
            read_file(descriptor);
            printf ("\nTotal of SIGUSR1 signals: %i", sigusr_cont);
            //printf ("hola%i %i %i\n", sigusr_cont, getpid(), getppid());
            break;
    }
}

void write_file (){
    
    char file[] = "stack_XXXXXX";
    descriptor = mkstemp (file);
    
    write (descriptor, stack, MAX);
    lseek (descriptor, 0, SEEK_SET);
}


void add_stack (int prime){
    
    stack = (int *) realloc (stack, (cont+1) * sizeof(int));
    *(stack + cont++) = prime;
}

void prime_numbers (){

    for (int prime = 1, i = 2; i < MAX; i++, prime = 1){
        for (int j = i/2; j > 1 && prime; j--)
            if (i % j == 0)
                prime = 0;
        if (prime)
            add_stack(i);
    }
    write_file();
    printf ("Child Pocress ID: %i Parent Process ID %i\n", getpid(), getppid());
}

int spawn (){

    pid_t child_pid;
    
    child_pid = fork();
    
    if (child_pid > 0)
        return child_pid;
    else{
        prime_numbers();
        //exit(0);//return 0;
    }
}

int main (int argc, char *argv[]) {

    struct sigaction sh;
    int child_status;

    memset (&sh, 0, sizeof (sh));
    sh.sa_handler = &events;
    
    sigaction (SIGINT, &sh, NULL);
    sigaction (SIGUSR1, &sh, NULL);
    sigaction (SIGCHLD, &sh, NULL);
   
    printf ("Proceso padre main %i\n", getpid());
    spawn();
    /*if (b > 0){
    while (wait (&child_status) < 0)
        sleep (1);
    }
    //while (1)
        //sleep(1);
    //waitpid (a, &child_status, 0);
    //wait (NULL);
        //wait (NULL);
    //while (wait (NULL) > 0);
    {*/
    //int a = spawn();
    //if (spawn() > 0){
    //if (spawn() == 0) 
     //   exit(0);
   // else{   
        printf ("Waiting to my child finish");
        while (wait3 (&child_status, WEXITED, NULL) < 0){//wait (&child_status) < 0){
            printf (".");        
            fflush(stdout);
            sleep (1); 
        }
    //}
    
    free(stack);

    return EXIT_SUCCESS;
}
