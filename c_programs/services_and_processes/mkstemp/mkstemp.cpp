#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main (int argc, char *argv[]) {
   
    char fname[] = "/tmp/fileXXXXXX";                           // Nombre de la plantilla (fichero temporal)
    int descriptor = mkstemp (fname);                           // Asignación del descriptor ("etiqueta" que hace referencia al nodo del fichero), mkstemp recibe como parametro un char *, no un const                                                                        char *, por lo que siemrpe la variable debe ser un array [], si es de tipo char *, el compilador la convierte a const automaticamente y f								   alla
    char *write_buffer = "Writing to tempfile";
    char read_buffer[strlen (write_buffer)]; 
    
    //memset(read_buffer,0,sizeof(read_buffer)); 
    
    printf ("%i\n", descriptor);

    write (descriptor, write_buffer, strlen (write_buffer));    // escribe en este archivo, lo que contiene char *, este numero de bytes
    lseek (descriptor, 0, SEEK_SET);                            // reposiciona el puntero del descriptor del fichero a la izquierda, para su posterior lestura
    read (descriptor, read_buffer, strlen (write_buffer));      // lee de este archivo, guardalo en este char *, este numero de bytes
   
    printf ("%s\n", read_buffer); 
    
    close (descriptor);
    unlink (fname);                                             // Elimina un nombre del sistema de archivos si hay un proceso usando el fichero lo podra seguir usando hasta que finalice


    return EXIT_SUCCESS;                
}
