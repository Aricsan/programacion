#ifndef __MULTI_H__
#define __MULTI_H__

#ifdef __cplusplus
extern "C"
{
#endif
	int mul (int, int);
	double divi (double, double);
#ifdef __cplusplus
}
#endif

#endif
