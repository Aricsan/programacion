#ifndef __SUM_H__
#define __SUM_H__

#ifdef __cplusplus
extern "C"
{
#endif
	int sum (int, int);
	int res (int, int);
#ifdef __cplusplus
}
#endif

#endif
