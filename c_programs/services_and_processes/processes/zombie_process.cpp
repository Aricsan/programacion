/*
 * =====================================================================================
 *
 *       Filename:  zombie_process.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20/10/20 19:28:11
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main (){
    
    pid_t child_pid;
    
    /* Crear proceso hijo. */
    child_pid = fork ();
    
    if (child_pid > 0) {
    /* El proceso padre se duerme creando un proceso zombi. */
        sleep (60);
    }
    else {
    /* El proceso hijo termina y devuelme el valor de retorno. */
        exit (0);
    }
    
    return 0;
}
