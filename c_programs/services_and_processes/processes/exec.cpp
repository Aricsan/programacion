/*
 * =====================================================================================
 *
 *       Filename:  exec.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20/10/20 20:23:18
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */

#include<stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int spawn (char* program, char** arg_list){
    
    pid_t child_pid;
    
    /* Crear un proceso nuevo. */
    child_pid = fork ();
    
    if (child_pid != 0)
    /* Proceso padre. */
        return child_pid;
    else {
    /* Cambiar la referencia de proceso padre y buscar el comando en el PATH del sistema. */
        execvp (program, arg_list);
    /* Mensaje de error solo si execvp devuelve un error. */
        fprintf (stderr, "a ocurrido un error con execvp\n");
        abort ();
    }
}

int main (){

    /* Lista de argumentos para el comando ls. */
    char* arg_list[] = {
        "ls",
        "-l",
        "/",
        NULL
};

    spawn ("ls", arg_list);
    printf ("Fin del proceso padre\n");
    
    return 0;
}
