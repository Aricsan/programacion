/*
 * =====================================================================================
 *
 *       Filename:  fibo_fork.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  25/10/20 12:48:52
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void fibo (int *firs_num, int *array){

    int number;
    printf ("fibo%i\n", *firs_num); 
    for (int i = 1; array[i] < (*firs_num)+1; i++){
        array = (int *) realloc (array, (i+2) * sizeof(int));
        array[i+1] = array[i] + array [i-1];
        printf ("%i\n", array[i+1]);
        number = array[i+1];
    }

    printf("number %i\n", number);
    *firs_num = number;
}

void spawn (int firs_num, int stop, int *array){

    pid_t child_pid;
    int child_status;

    printf ("fisr_num before fibo: %i\n", firs_num);

    for (int i = 0; array[i+1] < stop; i++){
        child_pid = fork(); // Devuelve el ID de proceso del proceso hijo
        if (child_pid != 0)
            wait(&child_status); // Devuelve el ID de proceso del hijo terminado
        else{
            fibo (&firs_num, array);
            printf ("%i\n", firs_num);
            exit (0);
        }
    }
}


int main (int argc, char *argv[]) {

    int *array = (int *) malloc (2);
    array[0] = 0;
    array[1] = 1;

    spawn (atoi(argv[1]), atoi(argv[2]), array);

    free (array);

    return EXIT_SUCCESS;
}

