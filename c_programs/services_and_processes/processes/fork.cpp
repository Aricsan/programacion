/*
 * =====================================================================================
 *
 *       Filename:  fork.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  20/10/20 20:05:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main (){
    pid_t child_pid;
    
    printf ("El ID de proceso del programa es: %i\n", (int) getpid ());
    child_pid = fork ();
    
    if (child_pid != 0) {
        printf ("Este es el proceso padre con el ID: %i\n", (int) getpid ());
        printf ("El ID del proceso hijo es: %i\n", (int) child_pid);
    }
    else
        printf ("Este es el proceso hijo con el ID: %i\n", (int) getpid ());
    
    return 0;
}
