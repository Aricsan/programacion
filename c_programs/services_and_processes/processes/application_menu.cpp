/*
 * =====================================================================================
 *
 *       Filename:  application_menu.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  21/10/20 19:22:21
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  ovi1knovi (), oviaricsan@gmail.com
 *   Organization:  none
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

void menu ();

void spawn (char *program_name, char **arg_list){
    
    pid_t child_pid = fork ();
    
    if (child_pid != 0)
        menu ();
    else{
        execvp (program_name, arg_list);
        fprintf (stderr, "execvp no se ha ejecutado correctamente\n");
        exit (1);
    }
}

void menu (){

    int option;
    char *arg_list[] = {NULL};

    do{
        printf ("\tElija que programa desea ejecutar \n");
        printf ("\t\t1 --> Firefox\n\t\t2 --> Gedit\n\t\t3 --> Sublime Text\n\t\t4 --> TexMaker\n\t\t5 --> Salir\n");
        scanf ("%u", &option);

        switch (option){
            case 1:
                spawn ("firefox", arg_list);
                break;
            case 2:
                 spawn ("gedit", arg_list);
                break;
            case 3:
                 spawn ("subl", arg_list);
                break;
            case 4:
                 spawn ("texmaker", arg_list);
                break;
            case 5:
                printf ("   --- Gracias por utilizar nuestros servicios ---\n\t\t--- Hasta Pronto ---\n");
                break;
            default :
                printf ("Opcion Incorrecta\n");
        }
    }while (option != 5);
}

int main (int argc, char *argv[]) {

    menu();

    return EXIT_SUCCESS;
}

