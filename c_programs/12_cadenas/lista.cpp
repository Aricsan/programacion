#include <stdio.h>
#include <stdlib.h>

const char *lista[] = {
    "Hola", 
    "Buenas Dias",
     NULL
};

int main (int argc, char *argv[]) {
    
    char frase[10];
    const char **p = lista;
    int celdas = sizeof(lista) / sizeof(char *);

    printf ("Introduce una palabra de como maximo 10 caracteres => ");
    scanf (" %s", frase);

    printf ("%s\n", frase); 
    
    printf ("Lista ocupa %li bytes\n", sizeof(lista));
    printf ("Contiene %li celdas\n", sizeof(lista) / sizeof(char *));
    
    for (int i=0; i<celdas; i++)
    printf ("La celda numero %i contiene %s\n", i, lista[i]);
    
    while (*p != NULL){ 
        printf ("%s\n", *p);
        p++;
    }

    return EXIT_SUCCESS;
}
