#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 0X100

const char * const cprog = "cadenas";
char vprog[] = "programa";

void concatenar (char *destino, const char *origen, int max){
    while (*(++destino) != 0);
    for (int i = 0; origen[i] != 0 && i < max; i++)
        destino[i] = origen[i];
    *destino = *origen;
}

void cifrar (char *mensaje, int clave){
    while (*mensaje != 0)
        *(++mensaje) += clave;//funcionamiento?
}

int main (int argc, char *argv[]) {
    
    char mensaje[MAX];
    //strcpy (mensaje, cprog);
    strncpy (mensaje, vprog, MAX);
    strcat (mensaje, " ");
    //strncat (mensaje, cprog, MAX - strlen(mensaje));
    
    concatenar (mensaje, cprog, MAX - strlen(mensaje));
    cifrar (mensaje, 3);
    printf ("%s\n", mensaje );//+ strlen (mensaje) + 1);
    cifrar (mensaje, -3);
    printf ("%s", mensaje);    

    return EXIT_SUCCESS;
}
