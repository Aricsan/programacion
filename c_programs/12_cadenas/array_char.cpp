#include <stdio.h>
#include <stdlib.h>

#define N 20

int main (int argc, char *argv[]) {
    
    unsigned char array[N];
    
    for (int i=0; i<N; i++)
        array[i] = (i+1) * (i+1);
    
    for(int i=0; i<N; i++)
        printf ("%i ", array[i]);

    return EXIT_SUCCESS;
}
