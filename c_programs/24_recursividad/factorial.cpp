#include <stdio.h>
#include <stdlib.h>

unsigned factorial (unsigned n){
    
    static unsigned res = 1;

    if (n > 0)
        res = n * factorial(n-1);

    return res;
}

int main (int argc, char *argv[]) {
    
    unsigned n;

    printf ("Introduce N: ");
    scanf (" %u", &n);

    printf ("El factorial de %u es %u\n", n, factorial(n));

    return EXIT_SUCCESS;
}
