#include <stdio.h>
#include <stdlib.h>

#define W 30

void impri_array (const char *array){
    
    if (*array != '\0'){
        printf ("%c", *array);
        impri_array(array+1);
    }
}

int main (int argc, char *argv[]) {

    const char *array = "Ricas Vacaciones";    

    impri_array(array);
    printf ("\n");

    return EXIT_SUCCESS;
}
