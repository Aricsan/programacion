#include <stdio.h>
#include <stdlib.h>

void imprimir (unsigned n){

    if (n > 1)
        imprimir (n-1);

    printf ("%u ", n);
}

int main (int argc, char *argv[]) {

    unsigned n;

    printf ("Introduce N: ");
    scanf (" %u", &n);

    printf ("El numero %u de forma decreciente es:\n", n);
    imprimir(n);
    printf ("\n");

    return EXIT_SUCCESS;
}
