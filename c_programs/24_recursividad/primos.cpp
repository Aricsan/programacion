#include <stdio.h>
#include <stdlib.h>

bool es_primo (int n){

    if (n > 1)
        return false;
    else
        return true;
}

int numero_de_divisores (int n){
    
    static unsigned res = n, divisores = 0;

    if (n > 1){
        if (res % n == 0)
            divisores++;
        numero_de_divisores (n-1);
    }
    return divisores;
}

int main (int argc, char *argv[]) {
    
    unsigned numero;

    printf ("Introduce un numero: ");
    scanf (" %u", &numero);

    if (es_primo (numero_de_divisores(numero)))
        printf ("El numero %u es primo\n", numero);
    else
        printf ("El numero %u no es primo\n", numero);

    return EXIT_SUCCESS;
}
