#include <stdio.h>
#include <stdlib.h>

#define N 10

/*unsigned suma (unsigned n){
    
    int resul = 0;

    for (int i=0; i<=n; i++)
        resul += i;

    return resul;
}*/

unsigned suma_r (unsigned n){

    if (n<1)
        return 0;
    
    return n + suma_r (n-1);// Realiza esta suma 10+9+8+7+6+5+4+3+2+1, recursivo se llama a si misma
}

int main (int argc, char *argv[]) {
    
    printf ("La suma de los %u primeros numeros es: %u\n", N, suma_r(N));

    return EXIT_SUCCESS;
}
