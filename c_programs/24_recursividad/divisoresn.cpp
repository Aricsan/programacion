#include <stdio.h>
#include <stdlib.h>

void divisores (unsigned n){
    
    static unsigned res = n;

    if (n > 0){
        if (res % n == 0)
            printf ("%u\n", n);
        divisores(n-1);
    }
}

int main (int argc, char *argv[]) {

    unsigned n;

    printf ("Introduce N: ");
    scanf (" %u", &n);

    printf ("Los dividores de %u son:\n", n);
    divisores(n);

    return EXIT_SUCCESS;
}
