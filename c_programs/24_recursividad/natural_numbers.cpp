#include <stdio.h>
#include <stdlib.h>

unsigned sumar (unsigned n){

    static unsigned res;
        
    if (n > 0)
        res = n + sumar(n-1);

    return res;
}

int main (int argc, char *argv[]) {

    unsigned n;

    printf ("Introduce N: ");
    scanf (" %u", &n);

    printf ("La suma de los %u numeros es: %u\n", n, sumar(n));

    return EXIT_SUCCESS;
}
