#include <stdio.h>
#include <stdlib.h>

unsigned factorial (unsigned n){
    
    if (n > 0)
        return n * factorial(n-1);

    return 1;
}

double potencia (double x, double exp){

    if (exp > 0)
        return x * potencia (x, exp-1);
    
    return 1;
}

void sn (double n){
    
    if (n >= 0){
        double res = potencia (-1, n) / factorial (n);
        sn (n-1);
        printf ("%.g ", res);
    }
}

int main (int argc, char *argv[]) {
    
    double n;

    printf ("Introduce N: ");
    scanf (" %lf", &n);
    
    printf ("s(%g) = ", n);
    sn (n);
    printf ("\n");

    return EXIT_SUCCESS;
}
