#include <stdio.h>
#include <stdlib.h>

int factorial (int n){

    int res = 1;

    if (n > 0)
        res = n * factorial(n-1);

    return res;
}

double e (double n){

    if (n > -1)    
        return 1/(double)factorial (n) + e (n-1);

    return 0;
}

int main (int argc, char *argv[]) {

    double n;

    printf ("Introduce e(n): ");
    scanf (" %lf", &n);

    printf ("EL valor de e(%g) es %lf\n", n, e (n));

    return EXIT_SUCCESS;
}
