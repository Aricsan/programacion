#include <stdio.h>
#include <stdlib.h>

#define E 0.1

int factorial (int n){

    if (n <= 0)
        return 1;
    
    return n * factorial(n-1);
}

double e (double n){

    double dif;
    
    dif = 1 / (double) factorial (n);

    if (dif < E)
        return dif;

    return dif + e (n+1); 
}

int main (int argc, char *argv[]) {

    printf ("EL valor es %g\n", e (0));

    return EXIT_SUCCESS;
}
