#include <stdio.h>
#include <stdlib.h>

const char *ordinal[3][10] = {
    { "", "primero", "segundo", "tercero", "cuarto", "quinto", "sexto", "séptimo", "octavo", "noveno" },
    { "", "décimo", "vigésimo", "trigésimo", "cuatrigésimo", "quincuagésimo", "sexagésimo", "septuagésimo", "octogésimo", "nonagésimo" },
    { "", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo", "quingentésimo", "sexgentésino", "septingentésimo", "octingentésimo", "noningentésimo" }
};

void num_ordinal (int num, int fila){

    if (fila <= 2){
        num_ordinal (num / 10, fila+1);
        printf ("%s ", ordinal[fila][num % 10]);
    }
}

int main (int argc, char *argv[]) {

    int num;

    printf ("Introduce el numero en cifras: ");
    scanf ("%i", &num);

    num_ordinal (num, 0);
    printf ("\n");

    return EXIT_SUCCESS;
}
