#include <stdio.h>
#include <stdlib.h>

#define W 30

void impri_array (const char *array){
    
    if (*array != '\0')
        impri_array (array+1);
    
    printf ("%c", *array);
}

int main (int argc, char *argv[]) {

    const char *array = "dabala arroz a la zorra el abad";    

    impri_array (array);
    printf ("\n");

    return EXIT_SUCCESS;
}
