#include <stdio.h>
#include <stdlib.h>

#define MAX 0x10

struct TPila {
    int data[MAX];
    int summit;
    int error;
};

void iniciar (struct TPila *data){
    data->error = 0;
    data->summit = 0;
}

void pedir_datos (struct TPila *pila){
    pila->error = 0;
    
    if (pila->summit >= MAX){
        pila->error = 1;
        return;
    }

    system ("clear");
    printf ("\n\t***Introduce numeros positivos en la pila***\n");

    do{
        printf ("\t\t--> ");
        scanf ("%i", &pila->data[pila->summit]);
    }while (pila->data[pila->summit++] >= 0);
    
    pila->summit--;
}

void mostrar_pila (struct TPila p){
    printf ("\n\t***Numeros***\n");

    for (int i=p.summit-1; i>=0; i--)
        printf ("\t\t--> %i\n", p.data[i]);
}

void mostrar_error (const char *fail){
    fprintf (stderr, "%s\n", fail);
}

int main (int argc, char *argv[]) {

    struct TPila pila;
    
    iniciar (&pila);
    pedir_datos (&pila);
    mostrar_pila (pila);

    return EXIT_SUCCESS;
}

