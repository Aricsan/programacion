#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "general.h"
#include "interfaz.h"

int main (int argc, char *argv[]) {

    struct TCoordenada velocidad, posicion;
    double t1, t2;

    velocidad = ask_user ("Velocidad");
    posicion = ask_user ("Posición");
    
    t2 = clock();
  
    while(1){
        t1 = clock();
        posicion.x += velocidad.x * (t2 / CLOCKS_PER_SEC - t1 / CLOCKS_PER_SEC);
        posicion.y += velocidad.y * (t2 / CLOCKS_PER_SEC - t1 / CLOCKS_PER_SEC);
        t2 = t1;
        mostrar (posicion, "Posicion");
        mostrar (velocidad, "Velocidad"); 
        usleep (1000000);
        system ("clear");
    }

    return EXIT_SUCCESS;
}

