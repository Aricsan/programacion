#ifndef __INTERFAZ_H__ 
#define __INTERFAZ_H__

#ifdef __cplusplus
extern "C"{
#endif

struct TCoordenada ask_user (const char *label);
void mostrar (struct TCoordenada data, const char *label);

#ifdef __cplusplus
}
#endif

#endif
