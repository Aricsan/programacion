#include <stdio.h>

#include "general.h"
#include "interfaz.h"

struct TCoordenada ask_user (const char *label) {
    struct TCoordenada collect;

    printf ("\n%s\n", label);
    printf ("Inserte un valor para x: ");
    scanf (" %lf", &collect.x);
    printf ("Inserte un valor para y: ");
    scanf (" %lf", &collect.y);

    return collect;
}

void mostrar (struct TCoordenada data, const char *label){
    
    printf ("\n%s", label);
    printf ("\t--> x %g\n\
             \t--> y %g\n", data.x, data.y);
}
