#include <stdio.h>

int main(){

    float a = 237E-3;//notacion cientifica
    printf("%.3f\n", a);
    /*float direccion_a;
    direccion_a = &a;
    float valor_de_offset;
    printf("El offset de a es: %f\n", direccion_a);
    valor_de_offset = *direccion_a;
    printf("El valor en memoria de a es: %f\n", valor_de_offset);
*/
    float b = -237E-3;
    printf("%.3f\n", b);

    float c = 237E-4;
    printf("%.4f\n", c);

    return 0;
}
