#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const char *command_name;

void manual_uso (FILE *output){
    
    int rv = 0;
    
    if (output == stderr)
        rv = 1;

    fprintf (output, "%s Es un programa destinado para ...:\n\
            Las opciones son las siguientes:\n\
            -h --> Mostrar Ayuda\n", command_name);
    
    exit (rv);
}

void get_opt (int argc, char *argv[]){

    char c;
    command_name = argv[0];

    if (argc < 2)
        manual_uso (stderr);

    while ((c = getopt (argc, argv, "h")) != -1)
        switch (c){
            case 'h':
                manual_uso (stdout);
                break;
            case '?':
                if (optopt == '.' || optopt == '.')
                    fprintf (stderr, "...\n");
                else
                    manual_uso (stderr);
                break;
        }
}

int main (int argc, char *argv[]) {
    
    get_opt (argc, argv);

    return EXIT_SUCCESS;
}

