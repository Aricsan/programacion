#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    unsigned numero;

    printf ("Introduce un numero: "); 
    scanf (" %u", &numero);

    for (int i=1; i<=numero; i++){
        if (numero % i == 0)
        printf ("El numero que divide %u es: %u\n", numero, numero / i);
    }

    for (int i=numero; i != 0; i--){
        printf ("\nLos divisores del %u son\n", i);
        for (int j=1; j<=i; j++)
            if (i % j == 0)
                printf (" %u ", j);
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
