#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    char *nombre;
    unsigned numero;
    unsigned numero_reves = 0;

    system ("clear");
    system ("toilet -f pagga --gay BINARIO");

    printf ("\n\n");
    printf ("Amadísimo usuario, ¿puedo conocer su nombre?\n");
    printf ("Nombre: ");
    scanf (" %ms", &nombre);

    printf ("%s espero que estés pasando un buen día.\n", nombre);
    free (nombre);

    printf ("Te voy a pasar un número a binario. A ver si acierto.\n");
    printf ("Número: ");
    scanf (" %u", &numero);

    for (int i=1; numero>0; numero>>=1, i*=10)
        numero_reves += (numero % 2) * i;

    printf ("%u", numero_reves);
    printf ("\n");

    return EXIT_SUCCESS;
}
