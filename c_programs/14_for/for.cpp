#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int numero = 7, potencia = 3;
    int resul_mul = 1, resul_sum = 0;

    for (int i=0; i<potencia; i++){
        resul_mul *= numero;
        for (int j=0; resul_sum<resul_mul; j++)
            resul_sum += numero;
    }
    
    printf ("El resultado multiplicando de 7^3 es: %d\nEl resultado sumando de 7^3 es: %d\n", resul_mul, resul_sum);

    return EXIT_SUCCESS;
}
