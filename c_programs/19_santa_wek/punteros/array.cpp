#include <stdio.h>
#include <stdlib.h>

#define W 5

void imprimir (int *numeros[W]){
    
    printf ("Numeros: ");
    for (int i=0; i<W; i++)
         printf ("%i ", *numeros[i]);
    printf ("\n");
}

int main (int argc, char *argv[]) {
    
    int numeros[W] = {4, 7, 2, 8, 1};
    int *num_ord[W];
    int *aux;

    for (int i=0; i<W; i++)
        num_ord[i] = &numeros[i];
    
    imprimir (num_ord);

    for (int i=0; i<W; i++)
         for (int j=i+1; j<W; j++)
             if (*num_ord[i] < *num_ord[j]){
                 aux = num_ord[j];
                 num_ord[j] = num_ord[i];
                 num_ord[i] = aux;
             }

    imprimir (num_ord);

    return EXIT_SUCCESS;
}
