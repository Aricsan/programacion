#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

void imprimir (const char *frase, char **palabras, int summit){
    
    printf ("\n\t%s\n", frase);
    for (int i=0; i<summit; i++)
        printf ("\t%s ", palabras[i]);
    printf ("\n\n");   
}

void liberar (char **palabras, int summit){

    for (int i=0; i<summit; i++)
         free (palabras[i]);

    free (palabras);
}

int main (int argc, char *argv[]) {

    char **palabras = NULL, *aux;
    int summit = 0;

    // Introducción de datos
    printf ("Introduce palabras separadas por espacios: ");
    do{
        palabras = (char **) realloc (palabras, (summit+1) * sizeof (char *));
        scanf (" %ms", &palabras[summit++]);
    }while (getchar() != '\n');

    imprimir ("Palabras Introducidas", palabras, summit);

    // Calculos de ordenación
    for (int i=0; i<summit-1; i++)
        for (int j=0; j<summit-1; j++)
            if (strcasecmp (palabras[j], palabras[j+1]) > 0){
                aux = palabras[j+1];
                palabras[j+1] = palabras[j];
                palabras[j] = aux;
            }

    imprimir ("Palabras Ordenadas", palabras, summit);

    liberar (palabras, summit);

    return EXIT_SUCCESS;
}

