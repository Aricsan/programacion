#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const char *command_name;

void manual_uso (FILE *output){//Cualquier tubo es un fichero
    
    int rv = 0;//Return Value
    
    if (output == stderr)
        rv = 1;

    fprintf (output, "%s Es un programa destinado para calcular el area de figuras:\n\
            Las opciones son las siguientes:\n\
            -h --> Mostrar Ayuda\n\
            -t --> <base> <altura> Calcular el Area de un Triangulo\n\
            -r --> <base> <altura> Calcular el Area de un Rectangulo\n", command_name);
    
    exit (rv);
}

double area_triangulo (double base, double altura){
    
    return (base * altura) / 2;
}

double area_rectangulo (double base, double altura){
    
    return base * altura;
}

int main (int argc, char *argv[]) {

    char c;

    command_name = argv[0];

    if (argc < 2)
        manual_uso (stderr);

    while ((c = getopt (argc, argv, "ht:r:")) != -1)//devuelve la cantidad argumentos que lee, cuando no hay mas devuelve -1
        switch (c){
            case 'h':
                manual_uso (stdout);
                break;
            case 't':
                printf ("El area del Triangulo con %s como base y %s como altura es: %g\n", argv[2], argv[3], area_triangulo (atof(argv[2]), atof(argv[3])));
                break;
            case 'r':
               printf ("EL area del Rectangulo con %s como base y %s como altura es: %g\n", argv[2], argv[3], area_rectangulo (atof(argv[2]), atof(argv[3])));
               break;
            case '?':
                if (optopt == 't' || optopt == 'r')//almacena el ultimo argumento antes de dar error
                    fprintf (stderr, "Las opciones -t y -r necesitan dos parametros\n"); 
                else
                    manual_uso (stderr);
                break;
        }

    return EXIT_SUCCESS;
}
