#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double pol_a (double li, double ls){

    double pol[] = {3, -2, 4, 3}, resul = 0, poten = 1;

    for (int x=li; x<ls; x++)
        for (int j=0; j<3; j++, poten*=x)
            resul += pol[j] * poten;

    return resul;
}

double pol_b (double li, double ls){

    double pol[] = {4, 5, 6}, resul = 0, poten = 1;

    for (int x=li; x<ls; x++)
        for (int j=0; j<2; j++, poten*=x)
            resul += pol[j] * poten;

    return resul;
}

double pol_c (double li, double ls){

    double pol[] = {-5, 4, 1, -3, 7}, resul = 0, poten = 1;

    for (int x=li; x<ls; x++)
        for (int j=0; j<4; j++, poten*=x)
            resul += pol[j] * poten;

    return resul;
}

int main (int argc, char *argv[]) {

    double (*fun[3]) (double, double) = {&pol_a, &pol_b, &pol_c};
    double li, ls; 
    int opcion;

    printf ("Seleccione el polinomio que desea calcular:\n\t 1 --> (3x^3 + 4x^2 -2x + 3)\n\t 2 --> (6x^2 + 5x + 4)\n\t 3 --> (7x^4 - 3x^3 +x^2 + 4x -5)\n");
    scanf ("%i", &opcion);
    printf ("Inserte el valor del Limite Inferior y Superior: ");
    scanf (" %lf %lf", &li, &ls);

    printf ("La x en el polinomio seleccionado y con %g como Limite Inferior y %g como superior es: %g\n",li, ls, (*fun[opcion-1]) (li, ls));

    return EXIT_SUCCESS;
}
