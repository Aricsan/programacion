#include <stdio.h>
#include <stdlib.h>

double area_triangulo (double base, double altura){
    
    return (base * altura) / 2;
}

double area_rectangulo (double base, double altura){
    
    return base * altura;
}

int main (int argc, char *argv[]) {
    
    double (*fun[2]) (double, double) = {&area_triangulo, &area_rectangulo};
    int opcion, base, altura;

    printf ("Que desea hacer:\n\t 1 --> Calcular el area de un Triangulo\n\t 2 --> Calcular el area de un Rectangulo\n");
    scanf ("%i", &opcion);
    printf ("Inserta la Base y la Altura de la Figura: ");
    scanf ("%i %i", &base, &altura);

    printf ("El area de la figura es: %g\n", (double) (*fun[opcion-1]) (base, altura));

    return EXIT_SUCCESS;
}
