#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double fun_a (double x){
    
    return pow (x, 2) + 3 * x -1;
}

double fun_b (double x){
    
    return 4 * pow (x, 3) - 2 * pow (x, 2) +3;
}

double fun_c (double x){

    return 6 * x + 3 * pow (x, 4) - 2;

}

int main (int argc, char *argv[]) {
    
    double (*fun[3]) (double) = {&fun_a, &fun_b, &fun_c};
    double x; 
    int opcion;

    printf ("Seleccione una función matemática:\n\t 1 --> x^2 + 3x -1\n\t 2 --> 4x^3 - 2x^2 + 3\n\t 3 --> 6x + 3x^4 -2\n");
    scanf ("%i", &opcion);
    printf ("Inserte un valor a x: ");
    scanf (" %lf", &x);

    printf ("El valor de la x en la función indicada es:  %g\n", (*fun[opcion-1]) (x));

    return EXIT_SUCCESS;
}
