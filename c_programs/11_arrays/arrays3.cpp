#include <stdio.h>
#include <ctype.h>

int a = 0, e = 0, i = 0, o = 0, u = 0;

int main () {
    
    char nombre[] = "Supercalifragilisticoespialidoso";
    
    for (int i = 0; nombre[i] != '\0'; i++){
        nombre[i] = tolower(nombre[i]);
        switch (nombre[i]){
            case 'a':
                a++;
                break;
            case 'e':
                e++;
                break;
            case 'i':
                ::i++;
                break;
            case 'o':
                o++;
                break;
            case 'u':
                u++;
        } 
    }   
    printf ("La frase %s tiene %i a %i e %i i %i o %i u\n", nombre, a, e, ::i, o, u);
    
    return 0;
}
