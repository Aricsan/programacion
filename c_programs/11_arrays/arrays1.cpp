#include <stdio.h>

#define R 2
#define G 1
#define B 0

int main () {
    
    unsigned char rgb[3];
    unsigned char mask[3];
    int con_numero[3];
    int numero = 0;
    int contador = 0;

    printf ("Introduce un valor 0 (ausencia total de color) o 1 (sin ausencia de color) para el color Rojo => ");
    scanf (" %c", &rgb[0]);
    fflush (stdin); 
    printf ("Introduce un valor 0 (ausencia total de color) o 1 (sin ausencia de color) ara el color Verde => ");
    scanf (" %c", &rgb[1]);
    
    printf ("Introduce un valor 0 (ausencia total de color) o 1 (sin ausencia de color) ara el color Azul => ");
    scanf (" %c", &rgb[2]);
    
    printf ("Introduce una mascara => ");
    scanf (" %s", mask);
    
    for (int i = 0; mask[i] != '\0'; i++){
        contador++;
        con_numero[i] = mask[i];
        
        if(contador == 1)
        numero = con_numero[0];
        
        if(contador == 2)
        numero = con_numero[0]*10 + con_numero[1];
        
        if(contador == 3)
        numero = con_numero[0]*100 + con_numero[1]*10 + con_numero[2];
    }
    printf ("El xor del color Rojo con la mascara %s es: %i\n", mask, rgb[0] == '1' ? 255 ^ numero : 0 ^ numero);
    
    printf ("El xor del color Verde con la mascara %s es: %i\n", mask, rgb[1] == '1' ? 255 ^ numero : 0 ^ numero);

    printf ("El xor del color Azul con la mascara %s es: %i\n", mask, rgb[2] == '1' ? 255 ^ numero : 0 ^ numero);
    
    return 0;
}
