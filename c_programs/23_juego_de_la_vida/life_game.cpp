#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/ioctl.h>
#include <ncurses.h>
#include <unistd.h>

#define ALTO 50
//X.ws_row
#define ANCHO 100
//X.ws_col
#define VECINOS 3

void imprimir (unsigned tablero[ALTO][ANCHO], unsigned alto, unsigned ancho){

    printf ("\n\t");
    for (int f=0; f<alto; f++){
        for (int c=0; c<ancho; c++)
            if (tablero[f][c] == 1)
                printf ("█");
            else
                printf (" ");

        printf ("\n\t");
    }
}

void rellenar (unsigned tablero[ALTO][ANCHO], unsigned alto, unsigned ancho){

    for (int f=0; f<ALTO; f++)
        for (int c=0; c<ANCHO; c++)
            tablero[f][c] = rand()%101; //Numero aleatorio entre 0 y 100, de esta manera disminuimos la probalidad de celulas vivas al 1%
}

int main (int argc, char *argv[]) {

    struct winsize X;
    ioctl(0, TIOCGWINSZ, &X);
    srand (time(NULL));

    unsigned vecinos = 0;
    unsigned tablero[ALTO][ANCHO];
    unsigned computo[ALTO][ANCHO];

    // Rellenar tablero con 1's y 0's
    rellenar (tablero, ALTO, ANCHO);

    for (int ronda=0;;ronda++){
        printf ("\n\tRonda\t%i\n", ronda);

        // Contar celulas vecinas    
        for (int f=0; f<ALTO; f++)
            for (int c=0; c<ANCHO; c++, vecinos=0){
                for (int f_vec=f-1; f_vec<=f+1; f_vec++)
                    for (int c_vec=c-1; c_vec<=c+1; c_vec++)
                        if (tablero[f_vec][c_vec] == 1 && 
                                (f_vec != f || c_vec != c) &&
                                (f_vec >= 0 && f_vec < ALTO) &&
                                (c_vec >= 0 && c_vec < ANCHO))
                            vecinos++;
                computo[f][c] = vecinos;
            }

        // Actualizar tablero con el computo
        for (int f=0; f<ALTO; f++)
            for (int c=0; c<ANCHO; c++){
                if (computo[f][c] == 2 ||
                        computo[f][c] == 3)
                    tablero[f][c] = 1;
                else
                    tablero[f][c] = 0;
            }

        imprimir (tablero, ALTO, ANCHO);
        sleep(1);
        system("clear");
    } 

    return EXIT_SUCCESS;
}
