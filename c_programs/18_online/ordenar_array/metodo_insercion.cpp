#include <stdio.h>
#include <stdlib.h>

double mediana (int *numeros, int cant){

    double resul;

    if ((cant % 2) == 0)
        resul = (double) (numeros[(cant / 2)-1] + numeros[cant / 2]) / 2;

    else
        resul = numeros[((cant+1)/2)-1];

    return resul;
}

int main (int argc, char *argv[]) {

    int *numeros, cant = 0;
    int aux;

    printf ("Introduce los numeros positivos que desees ordenar: ");
    do{
        numeros = (int *) realloc (numeros, (cant+1) * sizeof(int));
        scanf (" %i", &numeros[cant]);
    }while (numeros[cant++] >= 0);
    numeros = (int *) realloc (numeros, (cant-1) * sizeof(int));
    cant--;
    
/*    for (int i=0; i<cant; i++)
        for (int j=1; j<cant; j++)
            if (numeros[j] < numeros[j-1]){
                aux = numeros[j-1];
                numeros[j-1] = numeros[j];
                numeros[j] = aux;
            }*/

    for (int i=0; i<cant; i++)
        for (int j=i+1; j<cant; j++)
            if (numeros[i] > numeros[j]){
                aux = numeros[j];
                numeros[j] = numeros[i];
                numeros[i] = aux;
            }    
    
    
    printf ("\nNumeros introducidos ordenados\n");
    for (int i=0; i<cant; i++)
        printf ("%i ", numeros[i]);
    printf ("\n");

    printf ("\nLa mediana es: %g\n", mediana (numeros, cant));

    free (numeros);

    return EXIT_SUCCESS;
}
