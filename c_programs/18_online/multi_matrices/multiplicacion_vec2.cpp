#include <stdio.h>
#include <stdlib.h>

#define A 2
#define B 3
#define C 4

int main (int argc, char *argv[]) {

    double matriz_a[A][C]= {
        {5, 5, 7, 5},
        {2, 2, 1, 3}};

    double matriz_b[C][B] = {
        {3, 6, 1},
        {7, 4, 5},
        {8, 3, 4},
        {6, 2, 4}};

    double matriz_c[A][B];

    printf ("La multiplicación de las dos matrices es:\n");
    for (int f=0; f<A; f++){
        for (int c=0; c<B; c++){
            for (int m=0; m<C; m++)
                matriz_c[f][c] += matriz_a[f][m] * matriz_b[m][c];

            printf ("%4g", matriz_c[f][c]);
        }

        printf ("\n");
   }
    return EXIT_SUCCESS;
}
