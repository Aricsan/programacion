#include <stdio.h>
#include <stdlib.h>

#define D 3

int main (int argc, char *argv[]) {

    double matriz_a[D][D]= {
        {5, 2, 7},
        {2, 9, 3},
        {4, 7, 8}};
    
    double matriz_b[D][D] = {
        {3, 6, 1},
        {7, 4, 5},
        {8, 2, 4}};

   double matriz_c[D][D];

printf ("La multiplicación de las dos matrices es:\n");   
   for (int f=0; f<D; f++){
        for (int c=0; c<D; c++){
            for (int m=0; m<D; m++)
                matriz_c[f][c] += matriz_a[f][m] * matriz_b[m][c]; 

            printf ("%4g", matriz_c[f][c]);
        }
        
        printf ("\n");
   }

    return EXIT_SUCCESS;
}
