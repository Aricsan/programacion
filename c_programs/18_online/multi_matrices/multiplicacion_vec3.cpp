#include <stdio.h>
#include <stdlib.h>

void insert (double *matriz, char label, unsigned fil, unsigned col){
    
    printf ("Inserte los datos de la matriz %c:\n", label);
    for (int f=0; f<fil; f++)
        for (int c=0; c<col; c++)
            scanf (" %lf", matriz + (f * col + c));
}

void mul (double *matriz_a, double *matriz_b, double *matriz_c, unsigned m, unsigned k, unsigned n){
    
    for (int f=0; f<m; f++)
        for (int c=0; c<n; c++)
            for (int j=0; j<k; j++)
            *(matriz_c + (c+f*n)) += *(matriz_a + (j+f*k)) * *(matriz_b + (j*n+c)); 
}

void mostrar (double *matriz_c, unsigned fil, unsigned col){
        
        printf ("El resultado de la multiplicación de las matrices es:\n");
        for (int f=0; f<fil; f++){
            for (int c=0; c<col; c++)
                printf ("%3g", *(matriz_c + (f*col+c)));
            printf ("\n");
        }
}

int main (int argc, char *argv[]) {
    
    double *matriz_a, *matriz_b, *matriz_c;
    unsigned m, k, n;

    printf ("Inserte la cantidad de filas y columnas de la matriz A: ");
    scanf (" %u %u", &m, &k);

    printf ("Inserte la cantidad de columnas de la matriz B: ");
    scanf (" %u", &n);

    matriz_a = (double *) malloc ((m * k) * sizeof(double));
    matriz_b = (double *) malloc ((k * n) * sizeof(double));
    matriz_c = (double *) malloc ((m * n) * sizeof(double));
    
    insert (matriz_a, 'A', m, k);
    insert (matriz_b, 'B', k, n);

    mul (matriz_a, matriz_b, matriz_c, m, k, n);

    mostrar (matriz_c, m, n);
    free (matriz_a);
    free (matriz_b);
    free (matriz_c);

    return EXIT_SUCCESS;
}
