#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double pol (double *pol, int grado, double x){
    
    double result = 0, poten = 1;

    for (int i=0; i<grado; i++, poten*=x)
        result += pol[i] * poten;

    return result;
}
/*
bool zero (double *polinomio, unsigned grado, double li, double ls){

    bool resul;

    if ((pol (polinomio, grado, li) * pol (polinomio, grado, ls)) > 0)
        resul = true;
    else
        resul = false;

    return resul;
}
*/
bool zero (double li, double ls){

    bool resul;

    if ((li * ls) > 0)
        resul = true;
    else
        resul = false;

    return resul;
}

int main (int argc, char *argv[]) {

    double *polinomio = NULL, li, ls;
    unsigned i = 0, grado = 0;
    char stop;

    printf ("Introduce un polinomio:\n");
    scanf (" %*[(]");
    do{
        polinomio = (double *) realloc (polinomio, (i+1) * sizeof(double));
        grado += scanf (" %lf", &polinomio[i++]);
    }while(!scanf (" %[)]", &stop));

    do{
        printf ("Introduce el valor de li y ls con distinto signo: ");
        scanf (" %lf %lf", &li, &ls);
    }while (zero (li, ls));

    do{
        li /= 2;
        ls /= 2;
    }while (fabs (ls - li) > 1/100);

    return EXIT_SUCCESS;
}
