#include <stdio.h>
#include <stdlib.h>

double pol (double *polinomio, unsigned li, unsigned ls, unsigned incremento, unsigned grado){
    
    double resul, x = 1;
    
    for (int i=li; i<=ls; i+=incremento, resul=0, x=1){
        for (int j=0; j<grado; j++, x*=i)
            resul += polinomio[j] * x;
        
        printf ("El resultado del polinomio con li %u, ls %u e incremento %u es: %g\n", i, ls, incremento, resul);
    }
}

int main (int argc, char *argv[]) {
    
    double *polinomio = NULL;
    unsigned li, ls, x, incremento, i = 0, grado = 0;
    char stop;
    
    printf ("Introduce el polinomio:\n");
    do{
        polinomio = (double *) realloc (polinomio, (i+1) * sizeof(double));
        scanf (" %*[(]");
        grado += scanf (" %lf", polinomio+i++);
    }while (!scanf (" %[)]", &stop));
    
    printf ("Introduce el valor del Limite Inferior, Limite Superior e Incremento: ");
    scanf ("%u %u %u", &li, &ls, &incremento);
    
    pol (polinomio, li, ls, incremento, grado);
    
    free(polinomio);

    return EXIT_SUCCESS;
}
