#include <stdio.h>
#include <stdlib.h>

double potencia (double x, int indice){
    
    double result = 1;

    for (int i=0; i<indice; i++)
        result *= x;

    return result;
}

double pol (double *pol, int grado, double x){
   
    double result = 0; 
   
    for (int i=0; i<grado; i++)
       result += pol[i] * potencia(x, i);

    return result;
}

int main (int argc, char *argv[]) {

    double *polinomio = NULL;    
    unsigned i = 0, grado = 0, x;
    char stop;
    
    printf ("Introduce un polinomio:\n");
    scanf (" %*[(]");
    do{
        polinomio = (double *) realloc (polinomio, (i+1) * sizeof(double));
        grado += scanf (" %lf", &polinomio[i++]);
    }while(!scanf (" %[)]", &stop));
    
    printf ("Introduce el valor de x: ");
    scanf (" %u", &x);

    printf ("El resultado del polinomio es: %g\n %u\n", pol (polinomio, grado, x), i); 

    free (polinomio);    

    return EXIT_SUCCESS;
}
