#include <stdio.h>
#include <stdlib.h>

double pol (double *pol, int grado, double x){
 
    double result = 0, poten = 1;

    for (int i=0; i<grado; i++, poten*=x)
        result += pol[i] * poten;
 
    return result;
 }

bool zero (double *polinomio, unsigned grado, double li, double ls){
    
    bool resul;
        
    if ((pol (polinomio, grado, li) * pol (polinomio, grado, ls)) > 0)
        resul = true;
    else
        resul = false;
    
    return resul;
}

int main (int argc, char *argv[]) {

    double *polinomio = NULL, li, ls;
    unsigned i = 0, grado = 0;
    char stop;

    printf ("Introduce un polinomio:\n");
    scanf (" %*[(]");
    do{
        polinomio = (double *) realloc (polinomio, (i+1) * sizeof(double));
        grado += scanf (" %lf", &polinomio[i++]);
    }while(!scanf (" %[)]", &stop));

    printf ("Introduce el valor de li y ls: ");
    scanf (" %lf %lf", &li, &ls);

    printf ("El resultado del polinomio es: %g\n", pol (polinomio, grado, li));
    printf ("El resultado del polinomio es: %g\n", pol (polinomio, grado, ls));
    
    if (zero (polinomio, grado, li, ls))
        printf ("Los signos de li y ls son iguales\n");
    else
        printf ("Los signos de li y ls son distintos\n");

    free (polinomio);

    return EXIT_SUCCESS;
}
