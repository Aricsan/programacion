#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    unsigned numero;
    unsigned marca = 0;

    printf ("Introduce el numero que deseea comprobar: ");
    scanf (" %u", &numero);

    for (int i=2; i<numero; i++)
        if (numero % i == 0)
            marca++;
    
    if (marca)
        printf ("El numero %u no es primo\n", numero);
    else
        printf ("El numero %u es primo\n", numero);
    
    printf ("Los numeros primos menores que %u son ", numero);
    for (int i=numero-1; i>1; i--){
        marca = 0;
        for (int j=i-1; j>1; j--)
            if (i % j == 0)
                marca++;
        if (!marca)
            printf ("%u ", i);
    }
    
    printf ("\nLos 100 primeros numeros primos son: ");

    for (int i=2; i<=100; i++){
        marca = 0;
        for (int j=2; j<=100; j++)
            if (i != j && i % j == 0)
                marca++;
        if (!marca)
            printf ("\n%u ", i);
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
