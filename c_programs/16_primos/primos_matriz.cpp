#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
    
    unsigned matriz [10][10];
    unsigned rellenar = 1;
    unsigned marca = 0;
    unsigned division = 9;

    for (int i=0; i<10; i++){
        for (int j=0; j<10; j++){
            matriz [i][j] = rellenar++;
            printf ("%3u ", matriz[i][j]);
        }
        printf ("\n");
    }

    for (int i=2, marca=0; i<=100; i++)
        for (int j=2; j<=100; j++){
            if (i != j && i % j == 0)
                marca++;
            if (!marca)
                for (int z=9; z>1; z--)
                    if ((matriz [0][z] % matriz [0][1]) == 0)
                        for (int y=0; y<10; y++)
                            matriz [y][z] = 0;
                
        } 
    
    for (int i=0; i<10; i++){
        for (int j=0; j<10; j++)
            printf ("%3u ", matriz[i][j]); 
        printf ("\n");
    }

    return EXIT_SUCCESS;
}
