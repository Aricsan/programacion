#include <stdio.h>
#include <stdlib.h>

#define W 0.1

double pol (double *polinomio, double x, unsigned grado){

    double resul, poten = 1;

    for (int j=0; j<grado; j++, poten*=x)
        resul += polinomio[j] * poten; 

    return resul;
}

double integral (double *polinomio, double li, double ls, unsigned grado){
    
    double resul = 0;

    for (int x=li; x<ls; x++)
        resul += pol(polinomio, x, grado);
    
    return resul * W;
}

int main (int argc, char *argv[]) {
    
    double *polinomio = NULL, li, ls;
    unsigned i = 0, grado = 0;
    char stop;

    printf ("Introduce un polinomio:\n");
    scanf (" %*[(]");
    do{
        polinomio = (double *) realloc (polinomio, (i+1) * sizeof(double));
        grado += scanf (" %lf", &polinomio[i++]);
    }while(!scanf (" %[)]", &stop));

    printf ("Introduce el valor de li y ls: ");
    scanf (" %lf %lf", &li, &ls);

    printf ("El area del polinomio introducido es: %g\n", integral (polinomio, li, ls, grado));

    return EXIT_SUCCESS;
}
